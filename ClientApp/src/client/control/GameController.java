package client.control;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;

import bohr.engine.options.ServerProtocol;
//Imports del core del game engine
import bohr.engine.stage.Sprite;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;
import client.view.menu.GameFrame;
import server.model.game.CrazyBreackout;
import server.model.game.options.GameProtocol;
//Imports de proyecto del juego
import server.model.game.sprites.Bar;

public class GameController {

    private CrazyBreackout crazyBreakout;
    private GameFrame gameFrame;

    public GameController(CrazyBreackout game) {
        gameFrame = new GameFrame();
        this.crazyBreakout = game;
        //initNetwork();
    }

    public void start() {
        //game.getNetwork().listen();
        // engine.getUtil().loadAssets();
        initListeners(gameFrame.getGamePane(), startBar());
        crazyBreakout.getView().setGamePanel(//
                gameFrame.getGamePane());
        gameFrame.getGamePane().setSize(//
                crazyBreakout.getEngineState().getwScreen(), //
                crazyBreakout.getEngineState().gethScreen());
        crazyBreakout.start();
        /*game.getNetwork().sendEvent(ServerProtocol.START_GAME_COMMAND + " " + //
         game.getEngineState().getUser().getId());*/
    }

    private void initListeners(JPanel panel, final Bar gun) {
        gameFrame.getGamePane().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_RIGHT:
                        gun.move(2);
                        break;
                    case KeyEvent.VK_LEFT:
                        gun.move(4);
                        break;
                    case KeyEvent.VK_SPACE:
                        shoot();
                    default:
                        break;
                }
            }
        });
    }

    private void shoot() {
        /*try {
         for (Sprite s : game.getStage().getGameObjects()) {
         if (s.getId() > 1 && s.getX() + s.getWidth() > gun.getX()
         && s.getX() - s.getWidth() < gun.getX() && //
         s.getY() + s.getHeight() > gun.getY() && //
         s.getY() - s.getHeight() < gun.getY()) {
         game.getNetwork()
         .sendEvent(GameProtocol.DEL_DUCK_COMMAND + " " + //
         game.getEngineState().getUser().getId() + " " + //
         s.getId());
         }
         }
         } catch (Exception e2) {
         }*/
    }

    private Bar startBar() {
        Bar bar = new Bar(0, 0,
                crazyBreakout.getEngineState().gethScreen() - 20,
                0, 80, 20, 0, "src/images/bar1.png", 1) {
                    @Override
                    public void move(int type) {
                        /*game.getNetwork()
                         .sendEvent(GameProtocol.GUN_MOVE_COMMAND + " " + //
                         game.getEngineState().getUser().getId() + " " + //
                         type);*/
                        super.move(type);
                    }
                };
        crazyBreakout.getStage().addSprite(bar);
        return bar;
    }
    /*
     private void initNetwork() {
     game.getNetwork().addObserver(new Observer() {
     @Override
     public void update(Observable obs, String event, Object message) {
     try {
     if (event.equals(ServerProtocol.MESSAGES_COMMAND)) {
     gameFrame.getTxtChat()
     .setText(gameFrame.getTxtChat().getText() + //
     "BROADCAST: " + //
     ((JSONObject) message).getString("message")//
     + "\n");

     } else if (event.equals(ServerProtocol.MESSAGE_COMMAND)) {
     gameFrame.getTxtChat()
     .setText(gameFrame.getTxtChat().getText() + //
     "MESSAGE: " + //
     ((JSONObject) message).getString("message")//
     + "\n");
     } else if (event.equals(GameProtocol.UPDATE_SCORE_COMMAND)) {
     gameFrame.getResult()
     .setText(Integer.valueOf(((JSONObject) message).getInt("score")).toString());
     // gameFrame.getTxtChat().setText(Integer.valueOf(((JSONObject)
     // //
     // message).getInt("score")).toString());
     } else
     game.event(obs, event, (JSONObject) message);
     } catch (JSONException e) {
     e.printStackTrace();
     }
     }
     });
     }
     */

    public CrazyBreackout getGame() {
        return crazyBreakout;
    }

    public void setGame(CrazyBreackout game) {
        this.crazyBreakout = game;
    }

    public GameFrame getGameFrame() {
        return gameFrame;
    }

    public void setGameFrame(GameFrame gameFrame) {
        this.gameFrame = gameFrame;
    }

    private void initLocalWorld() {

    }

}
