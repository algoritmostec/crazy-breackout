package client.view.menu;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Fondo extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ImageIcon imagen;

	public Fondo(String nombre) {
		super();
		initialize();
		imagen = new ImageIcon(nombre);
		setSize(imagen.getIconWidth(), imagen.getIconHeight());
	}

	public Fondo() {
		super();
		initialize();
		imagen = new ImageIcon("");
	}

	public void loadImage(String nombre) {
		imagen = new ImageIcon(nombre);
		setSize(imagen.getIconWidth(), imagen.getIconHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		Dimension d = getSize();
		g.drawImage(imagen.getImage(), 0, 0, d.width, d.height, null);
		this.setOpaque(false);
		super.paintComponent(g);
	}

	private void initialize() {
		this.setSize(800, 600);
		this.setLayout(new GridBagConstraints());
	}

	private void setLayout(GridBagConstraints gridBagConstraints) {
	}
}
