package client.view.menu;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SetupPanel extends Fondo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bBack = new JButton("");
	private JButton bSave = new JButton("");
	private JLabel lblIp = new JLabel("IP: ");
	private JLabel lblPort = new JLabel("PORT: ");
	private JLabel lblScreenX = new JLabel("SCREEN X: ");
	private JLabel lblScreenY = new JLabel("SCREEN Y: ");	
	private JTextField textIp = new JTextField();
	private JTextField textPort = new JTextField();
	private JTextField textScreenX = new JTextField();

	private JLabel lblUser = new JLabel("USERNAME: ");
	private JTextField textUser = new JTextField();
	private JLabel lblPassword = new JLabel("PASSWORD: ");
	private JPasswordField textPassword = new JPasswordField();

	public JButton getbBack() {
		return bBack;
	}

	public JTextField getTextUser() {
		return textUser;
	}

	public void setTextUser(JTextField textUser) {
		this.textUser = textUser;
	}

	public JPasswordField getTextPassword() {
		return textPassword;
	}

	public void setTextPassword(JPasswordField textPassword) {
		this.textPassword = textPassword;
	}

	public void setbBack(JButton bBack) {
		this.bBack = bBack;
	}

	public JButton getbSave() {
		return bSave;
	}

	public void setbSave(JButton bSave) {
		this.bSave = bSave;
	}

	public JLabel getLblIp() {
		return lblIp;
	}

	public void setLblIp(JLabel lblIp) {
		this.lblIp = lblIp;
	}

	public JLabel getLblPort() {
		return lblPort;
	}

	public void setLblPort(JLabel lblPort) {
		this.lblPort = lblPort;
	}

	public JLabel getLblScreenX() {
		return lblScreenX;
	}

	public void setLblScreenX(JLabel lblScreenX) {
		this.lblScreenX = lblScreenX;
	}

	public JLabel getLblScreenY() {
		return lblScreenY;
	}

	public void setLblScreenY(JLabel lblScreenY) {
		this.lblScreenY = lblScreenY;
	}

	public JTextField getServerHost() {
		return textIp;
	}

	public void setTextIp(JTextField textIp) {
		this.textIp = textIp;
	}

	public JTextField getTextPort() {
		return textPort;
	}

	public void setTextPort(JTextField textPort) {
		this.textPort = textPort;
	}

	public JTextField getTextScreenX() {
		return textScreenX;
	}

	public void setTextScreenX(JTextField textScreenX) {
		this.textScreenX = textScreenX;
	}

	public JTextField getTextScreenY() {
		return textScreenY;
	}

	public void setTextScreenY(JTextField textScreenY) {
		this.textScreenY = textScreenY;
	}

	private JTextField textScreenY = new JTextField();

	public SetupPanel(String path) {
		super(path);
		setBounds(0, 0, 800, 600);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);

		lblUser.setForeground(new Color(255, 255, 255));
		lblUser.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblUser.setBounds(40, 15, 770, 105);
		this.add(lblUser);

		textUser.setBounds(300, 50, 300, 30);
		textUser.setColumns(10);
		this.add(textUser);

		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblPassword.setBounds(40, 65, 770, 105);
		this.add(lblPassword);

		textPassword.setBounds(300,105, 300, 30);
		textPassword.setColumns(10);
		this.add(textPassword);

		lblIp.setForeground(new Color(255, 255, 255));
		lblIp.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblIp.setBounds(40, 100, 770, 155);
		this.add(lblIp);

		textIp.setBounds(300, 160, 300, 30);
		this.add(textIp);
		textIp.setColumns(10);

		lblPort.setForeground(new Color(255, 255, 255));
		lblPort.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblPort.setBounds(40, 150, 770, 155);
		this.add(lblPort);

		textPort.setBounds(300, 210, 300, 30);
		this.add(textPort);
		textPort.setColumns(10);

		lblScreenX.setForeground(new Color(255, 255, 255));
		lblScreenX.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblScreenX.setBounds(40, 200, 770, 155);
		this.add(lblScreenX);

		textScreenX.setBounds(300, 260, 300, 30);
		this.add(textScreenX);
		textScreenX.setColumns(10);

		lblScreenY.setForeground(new Color(255, 255, 255));
		lblScreenY.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblScreenY.setBounds(40, 250, 770, 155);
		this.add(lblScreenY);

		textScreenY.setBounds(300, 310, 300, 30);
		this.add(textScreenY);
		textScreenY.setColumns(10);

		bSave.setIcon(new ImageIcon("src/images/menu/bSave.png"));
		bSave.setBounds(300, 400, 200, 50);
		this.add(bSave);
		
		bBack.setIcon(new ImageIcon("src/images/menu/bBack.png"));
		bBack.setBounds(518, 400, 200, 50);
		this.add(bBack);
	}

	public void initFields(String username, String password, //
			String ip, int port, int sizeX, int sizeY) {
		textIp.setText(ip);
		textPort.setText(Integer.valueOf(port).toString());
		textScreenX.setText(Integer.valueOf(sizeX).toString());
		textScreenY.setText(Integer.valueOf(sizeY).toString());
		textPassword.setText(password);
		textUser.setText(username);
	}

}
