package client.view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class FinishPanel extends Fondo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bComeBack = new JButton("");

	public FinishPanel(boolean finish) {
		super();
		loadImage(setFinishP(finish));
		
		setLayout(null);
		
		bComeBack.setIcon(new ImageIcon("src/images/menu/bComeBack.png"));
		;
		bComeBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		bComeBack.setBounds(300, 450, 200, 50);
		this.add(bComeBack);
	}

	public JButton getbComeBack() {
		return bComeBack;
	}

	public void setbComeBack(JButton bComeBack) {
		this.bComeBack = bComeBack;
	}

	private String setFinishP(boolean finish) {
		if (finish) {
			return "src/images/gameOver.jpg";
		}
		return "src/images/gameWin.jpg";

	}
}
