package client.view.menu;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

public class CreatePanel extends Fondo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bBack = new JButton("");
	private JButton bCreate = new JButton("");

	public CreatePanel(String path) {
		super (path);
		setBounds(0, 0, 800, 600);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);
		setVisible(true);

		bBack.setIcon(new ImageIcon("src/images/menu/bBack.png"));
		
		bBack.setBounds(518, 300, 200, 50);
		this.add(bBack);

		bCreate.setIcon(new ImageIcon("src/images/menu/bCreate.png"));
		bCreate.setBounds(75, 300, 200, 50);
		this.add(bCreate);
	}

	public JButton getbBack() {
		return bBack;
	}

	public void setbBack(JButton bBack) {
		this.bBack = bBack;
	}

	public JButton getbCreate() {
		return bCreate;
	}

	public void setbCreate(JButton bCreate) {
		this.bCreate = bCreate;
	}

}
