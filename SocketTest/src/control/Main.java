package control;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.Socket;
import java.net.SocketException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import events.Event;
import game.World;

public class Main {
	public static char[] getBuffer() {
		char buffer[] = new char[1024 * 6];
		for (int i = 0; i < buffer.length; i++)
			buffer[i] = ' ';
		return buffer;
	}

	public static void main(String args[]) {
		try {
			// Objetos para la comunicación y mensajeria
			Socket cliente = new Socket("127.0.0.1", 8888);
			PrintWriter out = new PrintWriter(cliente.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
			final Gson gson = new Gson();
			char buffer[] = getBuffer();

			// ***********************
			// Envío de evento
			Event eventJoin = new Event("join session", "", -1);
			out.println(gson.toJson(eventJoin));
			System.out.println(gson.toJson(eventJoin));
			// Lectura de respuesta al evento
			in.read(buffer);
			String inStr = String.valueOf(buffer).replace(" ", "");
			final World world = gson.fromJson(inStr, World.class);
			System.out.println(world.getSize());
			for (int i = 0; i < world.getBlocks().size(); i++) {
				System.out.println(world.getBlocks().get(i).getId());
			}

			// ***********************
			// Cerrado correcto del socket
			out.print("exit");
			System.out.println("exit");
			cliente.close();
			System.out.println("close");
		} catch (Exception ex) {

		}
	}
}
