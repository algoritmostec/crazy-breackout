package game;

public class Block {
	private int id;
	private int type;
	private boolean active;
	private int posX;
	private int posY;

	public Block(int id, int type) {
		this.id = id;
		this.type = type;
		active = true;
		posY = 0;
		posX = 0;
	}

	public Block() {
		this.id = -1;
		this.type = -1;
		active = false;
		posY = 0;
		posX = 0;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
