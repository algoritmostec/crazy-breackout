package bohr.engine.stage;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;

import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;

public class Sprite extends Observable implements Observer {

    protected float x, y, z;
    protected int width, height, angle, type;
    protected String asset;
    protected Image img;
    protected int id;
    // spritesheet, frame

    public Sprite() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.width = 0;
        this.height = 0;
        this.angle = 0;
        this.asset = "";
        this.type = 0;
        this.id = 0;
        this.img = null;
    }

    public Sprite(int id, float x, float y, float z, int width, int height, int angle, String asset, int type) {
        super();
        this.x = x;
        this.y = y;
        this.z = z;
        this.width = width;
        this.height = height;
        this.angle = angle;
        this.asset = asset;
        this.type = type;
        this.id = id;
        reloadImg();
    }

    public void paint(JPanel panel, Graphics g) {
        g.drawImage(img, (int) x, (int) y, width, height, panel);
    }

    public void step(int dt) {

    }

    public void move(float dx, float dy, float dz) {
        this.x += dx;
        this.y += dy;
        this.z += dz;
    }

    private void reloadImg() {
        img = Toolkit.getDefaultToolkit().getImage(asset);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
        reloadImg();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public void update(Observable obs, String msg, Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
