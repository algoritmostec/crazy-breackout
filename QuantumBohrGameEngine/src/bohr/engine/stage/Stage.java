package bohr.engine.stage;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import bohr.engine.GameEngine;
import bohr.engine.util.observable.Observable;

public class Stage extends Observable {

    private GameEngine engine;
    private Scene scene;
    private List<Sprite> gameObjects;

    public Stage(GameEngine engine) {
        this.engine = engine;
        setScene(new Scene());
        gameObjects = new ArrayList<>();
    }

    public void step(int dt) {
        for (Sprite s : gameObjects) {
            s.step(dt);
        }
    }

    public void render(Graphics g) {
        for (Sprite s : gameObjects) {
            s.paint(engine.getView().getGamePanel(), g);
        }
    }

    public void loadScene(Scene newScene) {
        setScene(newScene);
    }

    public void addSprite(Sprite sp) {
        gameObjects.add(sp);
    }

    public List<Sprite> getGameObjects() {
        return gameObjects;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
