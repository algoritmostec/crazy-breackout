package bohr.engine.stage;

import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.JPanel;

import bohr.engine.EngineState;
import bohr.engine.GameEngine;
import bohr.engine.util.observable.Observable;

public class View extends Observable {

    private GameEngine engine;
    private EngineState engineState;

    // Objetos de la pantalla
    private JPanel gamePanel;
    // private JFrame gameFrame;

    public View(GameEngine en) {
        engine = en;
        engineState = engine.getEngineState();
        initPanel();
    }

    private void initPanel() {
        gamePanel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                renderStage(g);
            }
        };
    }

    public void setGamePanel(JPanel contentPane) {
        contentPane.setSize(
                engineState.getwScreen(),
                engineState.gethScreen());
        gamePanel.setSize(
                engineState.getwScreen(),
                engineState.gethScreen());
        contentPane.add(gamePanel);
    }

    public void render() {
        gamePanel.repaint();
    }

    private void renderStage(Graphics g) {
        g.drawImage(
                Toolkit.getDefaultToolkit().
                        getImage("src/images/back.jpg"),
                0, 0, engineState.getwScreen(),
                engineState.gethScreen(), gamePanel
        );
        engine.getStage().render(g);
    }

    public JPanel getGamePanel() {
        return gamePanel;
    }

};
