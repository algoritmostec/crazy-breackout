package bohr.engine.util;

/**
 * Created by randall on 19/09/2015.
 */
public interface CallbackEvent {
    public void exec(String event);
}
