package bohr.engine.util.observable;

public interface Observer {
	public void update(Observable obs, String msg, Object obj);
}