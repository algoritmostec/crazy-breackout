package bohr.engine.util.observable;

import java.util.ArrayList;

/**
 * Clase observable
 */
public class Observable {

    private transient ArrayList<Observer> observers;

    public Observable() {
        observers = new ArrayList<Observer>();
    }

    public void addObserver(Observer obs) {
        observers.add(obs);
    }

    public void deleteObserver(Observer obs) {
        observers.remove(obs);
    }

    public void notifyObservers(Object obj) {
        for (Observer obs : observers) {
            obs.update(this, "", obj);
        }
    }

    public void notifyObservers(String msg, Object obj) {
        for (Observer obs : observers) {
            obs.update(this, msg, obj);
        }
    }

    public int countObservers() {
        return observers.size();
    }
}
