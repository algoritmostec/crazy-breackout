package bohr.engine;

import bohr.engine.game.GameLoop;
import bohr.engine.game.GameState;
import bohr.engine.input.Input;
import bohr.engine.net.Network;
import bohr.engine.stage.Scene;
import bohr.engine.stage.Stage;
import bohr.engine.stage.View;
import bohr.engine.util.Util;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;

/**
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class GameEngine extends Observable implements Observer {
    private GameLoop gameLoop;
    private GameState gameState;
    private EngineState engineState;
    // Por ahora solo hay soporte para el Stage
    private Stage stage;
    private Network network;
    private Input input;
    private Util util;
    private View view;

    public GameEngine() {
        super();
        engineState = new EngineState();
        gameState = new GameState();
        input = new Input();
        util = new Util();
        network = new Network(this);
        stage = new Stage(this);
        gameLoop = new GameLoop(this);
        view = new View(this);
    }

    public void setSceneOnStage(Scene scGameShooting) {
        stage.loadScene(scGameShooting);
		// Se inicia el juego cuando se establece una escena
        // si no se ha iniciado
        if (!gameLoop.getGameThread().isAlive()) {
            gameLoop.getGameThread().start();
        }
    }

    public void start() {
        if (!gameLoop.getGameThread().isAlive()) {
            gameLoop.getGameThread().start();
        }
    }

    // GETTERS AND SETTERS
    public EngineState getEngineState() {
        return engineState;
    }

    public GameLoop getGameLoop() {
        return gameLoop;
    }

    public void setGameLoop(GameLoop gameLoop) {
        this.gameLoop = gameLoop;
    }

    public GameState getGameSetup() {
        return gameState;
    }

    public void setGameSetup(GameState gameSetup) {
        this.gameState = gameSetup;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public Util getUtil() {
        return util;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void update(Observable obs, String msg, Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
