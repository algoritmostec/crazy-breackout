package bohr.engine.net;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import bohr.engine.GameEngine;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.Request;
import bohr.engine.util.observable.Observable;

/**
 * Clase encargada de la conexión con los socket, se encarga de la entrada y salida de la informacion.
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class Network extends Observable {
    private GameEngine engine;
    private Thread networkThread;
    private Socket connection;
    private PrintWriter out;
    private BufferedReader in;


    public Network(final GameEngine engine) {
        this.engine = engine;
        this.connection = null;
        this.out = null;
        this.in = null;
    }

    /**
     * Metodo encargado de abrir la conexión con el socket
     */
    public void openConnection() {
        try {
            this.connection = new Socket(engine.getEngineState().getServerHost(),
                    engine.getEngineState().getPort());
            out = new PrintWriter(connection.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (IOException e) {
            System.out.println("Error while creating socket: " + e);
        }
    }

    /**
     * Metodo encargado de cerrar la conexión con el socket
     */
    public void closeConnection() {
        sendEvent(new Gson().toJson(new Request(CommunicationProtocol.EXIT, "")));
        if (out != null)
            out.close();
        if (in != null)
            try {
                in.close();
            } catch (IOException e) {
            }
        if (connection != null)
            try {
                connection.close();
            } catch (IOException e) {
            }
    }

    public boolean isServerOn() {
        return (this.connection != null &&
                this.connection.isConnected());
    }

    /**
     * Metodo encargado de enviar eventos
     * @param event el evento
     * @return
     */
    public String sendEvent(String event) {
        String response = null;
        try {
            synchronized (this.connection) {
                out.println(event);
                response = in.readLine();
            }
        } catch (IOException e) {
            System.out.println("Error while sending event: " + e);
        }
        return response;
    }

    /**
     * Metodo encargado de mantener la conexion para escuchar los eventos.
     */
    private void listen() {
        final Network net = this;
        networkThread = new Thread() {
            @Override
            public void run() {
                // EVENTS GAME RESQUEST
                //listenEvents(engine.getEngineState().getSessionID(), out, in);
            }

            private void listenEvents(int idSession, PrintWriter out, BufferedReader in) {
                // EVENTS GAME RESQUEST
                System.out.println("events game " + idSession);
                out.println("events game " + idSession);
                String response = "";
                /*while (!(response = getResponseStr(in)).equals(ServerProtocol.EXIT)) {
                    JSONObject jsonResponse = new Util().strToJSONObject(response);
					//System.out.println(jsonResponse.toString());
					try {
						net.notifyObservers(jsonResponse.getString("event"), jsonResponse.getJSONObject("message"));
					} catch (JSONException e) {
						System.out.println("Error while parsing event.");
					}
				}*/
            }
        };
        networkThread.start();
    }

    public Socket getConnection() {
        return connection;
    }
}
