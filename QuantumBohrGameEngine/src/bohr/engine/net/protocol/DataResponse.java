package bohr.engine.net.protocol;

/**
 *
 * Protocolo de respuestas de solicitudies de datos
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class DataResponse<T> {
    private T data;
    String response;
    int code;

    /**
     * Constructor
     * @param data el dato de la respuesta
     */
    public DataResponse(T data) {
        this.response = CommunicationProtocol.OK;
        this.code = CommunicationProtocol.OK_CODE;
        this.data = data;
    }

    /**
     * Responde un codigo
     * @param response La respuesta
     * @param code y el codigo
     */
    public DataResponse(String response, int code) {
        this.response = response;
        this.code = code;
        this.data = null;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
