package bohr.engine.net.protocol;

/**
 * Protocolos de comunicacion con el servidor
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class CommunicationProtocol {
    //MESSAGES
    public static final String OK = "ok";
    public static final String ERROR = "error";
    public static final String EXIT = "exit";

    //SERVER CODES
    public static final int OK_CODE = 200;
    public static final int ERROR_CODE = 201;
    public static final int NOT_FOUND = 404;

    //DAO CODES
    public static final int ALREADY_CREATED = 800;
    public static final int ALREADY_VOTED = 801;

    //*********SERVICES**************//
    //USER
    public static final String USER_SERVICE = "user";
    public static final String SIGN_UP = "signUp";
    public static final String GET_ALL = "getAll";
    public static final String GET = "get";
    public static final String LOGIN = "login";
    public static final String GET_ALL_BY_USER = GET_ALL + "ByUser";
    //CLAN
    public static final String CLAN_SERVICE = "clan";
    public static final String CREATE = "create";
    public static final String CLAN_UPDATE = "clanUpdate";
    public static final String GET_ALL_BY_CLAN = GET_ALL + "ByClan";
    //GAME RESOURCES
    public static final String GAME_RESOURCE_SERVICE = "gameResource";
    public static final String COLLECT = "collect";
    //GAME REQUESTS
    public static final String GAME_REQUESTS_SERVICE = "gameRequests";
    public static final String SEND = "send";
    public static final String RESPONSE = "response";

    //GAME VOTING
    public static final String GAME_ACTION_SERVICE = "gameAction";
    public static final String BUY = "buy";
    public static final String PANDORA_UNDER_ATTACK = "pandoraUnderAttack";

    //GAME MESSAGING
    public static final String GAME_MESSAGING_SERVICE = "gameMessaging";
}
