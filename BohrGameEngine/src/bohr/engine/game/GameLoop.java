package bohr.engine.game;

import java.util.Date;

import bohr.engine.GameEngine;
import bohr.engine.util.observable.Observable;

public class GameLoop extends Observable {
	private GameEngine engine;
	private Thread gameThread;
	private Date lastUpdate;
	private int interval; // miliseconds

	public GameLoop(final GameEngine engine) {
		this.engine = engine;
		interval = 10;
		setGameThread();
	}

	private void setGameThread() {
		final GameLoop gL = this;
		gameThread = new Thread() {
			@Override
			public void run() {
				GameState gameState = engine.getGameSetup();
				lastUpdate = new Date();
				int dt = 0;
				while (!gameState.isFinished()) {
					// Notifica comienzo de un loop
					gL.notifyObservers("preloop", null);

					// Tiempo transcurrido entre el update anterior y el actual
					dt = (int) (new Date().getTime() - lastUpdate.getTime());

					// Actualiza los objetos del stage
					gL.notifyObservers("step", dt);
					engine.getStage().step(dt);

					// Repinta los objetos del stage
					// Repinta la pantalla del juego
					engine.getView().render();

					// Marca de la �ltima actualizaci�n
					lastUpdate = new Date();

					// Notifica fin de un loop
					gL.notifyObservers("endloop", null);

					sleep();

					// System.err.println(dt);
				}
			}

			private void sleep() {
				try {
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					System.out.println("sleepError: " + e.getMessage());
				}
			}
		};
	}

	public Thread getGameThread() {
		return gameThread;
	}

	public void setGameThread(Thread gameThread) {
		this.gameThread = gameThread;
	}

}
