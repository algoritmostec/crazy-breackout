package bohr.engine;

import bohr.engine.admin.User;

/**
 * Clase encargada del juego en general desde las entradas de usuario,
 * contrasella, hasta la pantalla y el puerto de entrada
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class EngineState {
	private int wScreen;
	private int hScreen;
	private String serverHost;
	private String username;
	private String password;
	private int port;
	private User user;

	/**
	 * Constructor
	 */
	public EngineState() {
		wScreen = 755;
		hScreen = 455;
		serverHost = "192.168.43.188";
		// serverHost = "127.0.0.1";
		username = "";
		password = "";
		port = 8080;
		user = new User();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getwScreen() {
		return wScreen;
	}

	public void setwScreen(int wScreen) {
		this.wScreen = wScreen;
	}

	public int gethScreen() {
		return hScreen;
	}

	public void sethScreen(int hScreen) {
		this.hScreen = hScreen;
	}

}
