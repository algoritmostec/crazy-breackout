package bohr.engine.net;

public class Event {
	private String event;
	private String type;
	private int userId;

	public Event(String event, String type, int userId) {
		this.type = type;
		this.event = event;
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
