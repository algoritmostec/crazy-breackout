package bohr.engine.net;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import bohr.engine.GameEngine;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.Request;
import bohr.engine.util.observable.Observable;

/**
 * Clase encargada de la conexión con los socket, se encarga de la entrada y
 * salida de la informacion.
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class Network extends Observable {
	private GameEngine engine;
	private Thread networkThread;
	private Socket connection;
	private PrintWriter out;
	private BufferedReader in;
	private final Gson gson = new Gson();

	public Network(final GameEngine engine) {
		this.engine = engine;
		this.connection = null;
		this.out = null;
		this.in = null;
	}

	/**
	 * Metodo encargado de abrir la conexión con el socket
	 */
	public void openConnection() {
		try {
			this.connection = new Socket(engine.getEngineState().getServerHost(), engine.getEngineState().getPort());
			out = new PrintWriter(connection.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		} catch (IOException e) {
			System.out.println("Error while creating socket: " + e);
		}
	}

	/**
	 * Metodo encargado de cerrar la conexión con el socket
	 */
	public void closeConnection() {
		sendEvent(new Gson().toJson(new Request(CommunicationProtocol.EXIT, "")));
		if (out != null)
			out.close();
		if (in != null)
			try {
				in.close();
			} catch (IOException e) {
			}
		if (connection != null)
			try {
				connection.close();
			} catch (IOException e) {
			}
	}

	public boolean isServerOn() {
		return (this.connection != null && this.connection.isConnected());
	}

	/**
	 * Metodo encargado de enviar eventos
	 * 
	 * @param event
	 *            el evento
	 * @return
	 */
	public String sendEvent(String event) {
		String response = null;
		try {
			synchronized (this.connection) {
				out.println(event);
				response = in.readLine();
			}
		} catch (IOException e) {
			System.out.println("Error while sending event: " + e);
		}
		return response;
	}

	public char[] getBuffer() {
		char buffer[] = new char[1024 * 6];
		for (int i = 0; i < buffer.length; i++)
			buffer[i] = ' ';
		return buffer;
	}

	public String sendEventBuffer(String event) {
		/*
		 * System.out.println("****************************");
		 * System.out.println("****************************");
		 * System.out.println(event);
		 * System.out.println("****************************");
		 * System.out.println("****************************");
		 */
		char buffer[] = new char[1024 * 6];
		for (int i = 0; i < buffer.length; i++)
			buffer[i] = ' ';
		try {
			synchronized (this.out) {
				out.println(event);
				in.read(buffer);
			}
		} catch (IOException e) {
			System.out.println("Error while sending event: " + e);
		}
		return String.valueOf(buffer);
	}

	/**
	 * Metodo encargado de mantener la conexion para escuchar los eventos.
	 */
	public void listen() {
		final Network net = this;
		networkThread = new Thread() {
			@Override
			public void run() {
				// EVENTS GAME RESQUEST
				listenEvents(-1, out, in);
			}

			private void listenEvents(int idSession, PrintWriter out, BufferedReader in) {
				// EVENTS GAME RESQUEST
				Socket con = null;
				PrintWriter out1 = null;
				BufferedReader in1 = null;
				try {
					con = new Socket(engine.getEngineState().getServerHost(), engine.getEngineState().getPort());
					out1 = new PrintWriter(con.getOutputStream(), true);
					in1 = new BufferedReader(new InputStreamReader(con.getInputStream()));
				} catch (IOException e) {
					System.out.println("Error while creating socket: " + e);
				}
				Event notify = new Event("notification", "", -1);
				out1.println(gson.toJson(notify));
				while (con.isConnected()) {
					char[] response = getBuffer();
					try {
						in1.read(response);
						net.notifyObservers(String.valueOf(response), null);
						// sleep(100);
					} catch (Exception ex) {
						System.out.println(String.valueOf(response));
						System.out.println("Exception: " + ex.getLocalizedMessage());
					}

				}
			}
		};
		networkThread.start();
	}

	public Socket getConnection() {
		return connection;
	}
}
