package bohr.engine.options;

public class ServerProtocol {
	// REGEX
	// final private static String SPACES = "[x0Bf]*";
	// final private static String ALFA_NUM = "\\{alnum\\}+";
	final private static String ALFA_NUM = "[a-zA-Z0-9]+";
	final private static String NUM = "[0-9]+";
	final private static String TWO_ALFA_NUM = "(" + ALFA_NUM + ") (" + ALFA_NUM + ")";

	// SERVER
	final public static String HELLO = "hello";
	final public static String EXIT = "exit";
	final public static String OK = "ok";

	// SESSIONS
	final public static String CREATE_SESSION = "create session";
	final public static String GET_SESSIONS = "get sessions";
	final public static String GET_SESSION = "get session";
	final public static String GET_SESSION_REGEX = GET_SESSION + " (" + NUM + ")";
	final public static String UPDATE_SESSIONS = "update session";
	final public static String UPDATE_SESSIONS_REGEX = UPDATE_SESSIONS;

	// PLAYERS
	final public static String GET_PLAYERS = "get players";
	final public static String CREATE_PLAYER = "create player";
	final public static String CREATE_PLAYER_REGEX = CREATE_PLAYER+" " + TWO_ALFA_NUM;

	// GAME
	final public static String CREATE_GAME_REGEX = "create game" + " (" + NUM + ")";
	final public static String START_GAME_COMMAND = "start game";
	final public static String START_GAME_REGEX = START_GAME_COMMAND + " (" + NUM + ")";
	// final public static String START_GAME_REGEX = START_GAME + TWO_ALFA_NUM;
	final public static String GET_ALL_GAME_COMMAND = "get all game";
	final public static String GET_ALL_GAME = GET_ALL_GAME_COMMAND + " (" + NUM + ")";
	final public static String EVENTS_GAME = "events game" + " (" + NUM + ")";
	final public static String JOIN_GAME = "join game";
	final public static String JOIN_GAME_REGEX = JOIN_GAME + " (" + NUM + ") " + TWO_ALFA_NUM;

	final public static String MESSAGE_COMMAND = "message client";
	final public static String MESSAGE_REGEX = MESSAGE_COMMAND + " (" + NUM + ")" + " (" + ALFA_NUM + ")";

	final public static String MESSAGES_COMMAND = "message clients";
	final public static String MESSAGES_REGEX = MESSAGES_COMMAND + " (" + ALFA_NUM + ")";
	
	

}