package client.control;

import java.util.List;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.StringReader;
import java.util.ArrayList;

import javax.management.StringValueExp;
import javax.swing.JPanel;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import bohr.engine.admin.User;
//Imports del core del game engine
import bohr.engine.stage.Sprite;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;
import client.view.menu.GameFrame;
import game.model.events.*;
import game.model.entities.world.Ball;
import game.model.entities.world.Bar;
import game.model.entities.world.Block;
import game.model.entities.world.World;
import server.model.game.CrazyBreackout;
import server.model.game.options.GameProtocol;

//Imports de proyecto del juego

public class GameController {

	private CrazyBreackout crazyBreakout;
	private GameFrame gameFrame;
	private final Gson gson = new Gson();

	public GameController(CrazyBreackout game) {
		gameFrame = new GameFrame();
		this.crazyBreakout = game;
		crazyBreakout.getNetwork().listen();
		initNetwork();
	}

	private void wordUpdate() {
		Event eventJoin = new Event("get world", "1", -1);
		Event eventJoin1 = new Event("get world", "2", -1);
		Event eventJoin2 = new Event("get world", "3", -1);
		Event eventJoin3 = new Event("get world", "4", -1);
		final World world = gson.fromJson(crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(eventJoin)),
				World.class);
		final World world1 = gson.fromJson(crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(eventJoin1)),
				World.class);
		final World world2 = gson.fromJson(crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(eventJoin2)),
				World.class);
		final World world3 = gson.fromJson(crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(eventJoin3)),
				World.class);
		crazyBreakout.setWorld(world);
		crazyBreakout.setWorld(world3);
		crazyBreakout.setWorld(world1);
		crazyBreakout.setWorld(world2);
	}

	public void start() {
		Event eventId = new Event("join session", "", -1);
		crazyBreakout.getEngineState().getUser().setId(gson
				.fromJson(crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(eventId)), Event.class).getUserId());
		Event eventClients = new Event("get clients", "", -1);
		List<Double> users = gson.fromJson(crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(eventClients)),
				List.class);
		setUsers(users);
		wordUpdate();
		// engine.getUtil().loadAssets();
		starBall(crazyBreakout.getEngineState().getUser().getId());
		initListeners(gameFrame.getGamePane(), startBar(crazyBreakout.getEngineState().getUser().getId()));
		crazyBreakout.getView().setGamePanel(//
				gameFrame.getGamePane());
		gameFrame.getGamePane().setSize(//
				crazyBreakout.getEngineState().getwScreen(), crazyBreakout.getEngineState().gethScreen());
		crazyBreakout.getGameLoop().addObserver(new Observer() {

			@Override
			public void update(Observable obs, String msg, Object obj) {
				if (msg.equals("preloop")) {
					/*
					 * new Thread() { public void run() {
					 */
					checkCollision(searchBloks().length,
							(Ball) searchSpriteByTyppe(2, crazyBreakout.getEngineState().getUser().getId()),
							(Bar) searchSpriteByTyppe(1, crazyBreakout.getEngineState().getUser().getId()),
							searchBloks());
					/*
					 * } }.start();
					 */
				}
			}
		});

		crazyBreakout.getNetwork().addObserver(new Observer() {

			@Override
			public void update(Observable obs, String msg1, Object obj) {
				String msg = msg1.trim();
				// System.out.println("******************************");
				// System.out.println(msg);
				// System.out.println("******************************");
				Event event = gson.fromJson(msg, Event.class);

				if ("join session".equals(event.getEvent())) {
					starBall(event.getUserId());
					startBar(event.getUserId());
				} else if ("gameover".equals(event.getEvent())) {
					Ball ball = (Ball) searchSpriteByTyppe(2, event.getUserId());
					if (crazyBreakout.getEngineState().getUser().getId() != event.getUserId()) {
						ball.setX(1000000000);
						ball.setXdir(0);
						ball.setXdir(0);
						ball.setActive(false);

					}
				} else if ("hit block".equals(event.getEvent())) {
					HitBlockEvent hitBlock = gson.fromJson(msg, HitBlockEvent.class);
					Block brick = (Block) searchSpriteByTyppe(0, hitBlock.getBlockId());
					brick.setActive(false);
					brick.setX(2000000);
					gameFrame.getResult()
							.setText(String.valueOf((Integer.valueOf(gameFrame.getResult().getText()) + 1)));

					if (crazyBreakout.getEngineState().getUser().getId() != hitBlock.getUserId()) {
						Ball ball = (Ball) searchSpriteByTyppe(2, hitBlock.getUserId());
						ball.setX(hitBlock.getX());
						ball.setY(hitBlock.getY());
						ball.setXdir(hitBlock.getxDir());
						ball.setYdir(hitBlock.getyDir());
					}

				} else if ("ball hit".equals(event.getEvent())) {
					HitBlockEvent hitBlock = gson.fromJson(msg, HitBlockEvent.class);
					Ball ball = (Ball) searchSpriteByTyppe(2, hitBlock.getUserId());
					if (crazyBreakout.getEngineState().getUser().getId() != hitBlock.getUserId()) {
						ball.setX(hitBlock.getX());
						ball.setY(hitBlock.getY());
						ball.setXdir(hitBlock.getxDir());
						ball.setYdir(hitBlock.getyDir());
					}
				} else if ("bar move".equals(event.getEvent())) {
					HitBlockEvent hitBlock = gson.fromJson(msg, HitBlockEvent.class);
					Bar bar = (Bar) searchSpriteByTyppe(1, hitBlock.getUserId());
					if (crazyBreakout.getEngineState().getUser().getId() != hitBlock.getUserId()) {
						bar.setX(hitBlock.getX());
						bar.setY(hitBlock.getY());
					}
				}

			}
		});

		crazyBreakout.start();
		/*
		 * game.getNetwork().sendEvent(ServerProtocol.START_GAME_COMMAND + " " +
		 * // game.getEngineState().getUser().getId());
		 */
	}

	private Ball starBall(int userId) {
		if (userId != crazyBreakout.getEngineState().getUser().getId()) {
			Ball ball = new Ball(userId, 200, 300, 500, 50, 50, 1, "src/images/ball.png", 2);
			crazyBreakout.getStage().addSprite(ball);
			return ball;
		} else {
			Ball ball = new Ball(userId, 200, 300, 500, 50, 50, 1, "src/images/ball1.png", 2);
			crazyBreakout.getStage().addSprite(ball);
			return ball;
		}

	}

	private Sprite searchSpriteByTyppe(int type, int id) {
		for (Sprite sprite : crazyBreakout.getStage().getGameObjects()) {
			if (sprite.getType() == type && sprite.getId() == id) {
				return sprite;
			}
		}
		return null;
	}

	private void checkCollision(int N_OF_BRICKS, final Ball ball, final Bar paddle, final Block[] bricks) {
		/*
		 * if (ball.getRect().getMaxY() >
		 * crazyBreakout.getEngineState().gethScreen() && ball.isActive()) {
		 * ball.setX(1000000000); ball.setXdir(0); ball.setXdir(0);
		 * ball.setActive(false); new Thread() { public void run() {
		 * crazyBreakout.getNetwork().sendEventBuffer( gson.toJson(new Event(
		 * "game over", "", crazyBreakout.getEngineState().getUser().getId())));
		 * } }.start(); }
		 */
		if (ball.getX() == 0) {
			ball.setXdir(1);
			new Thread() {
				public void run() {
					crazyBreakout.getNetwork()
							.sendEventBuffer(gson.toJson(
									new HitEvent("ball hit", "", crazyBreakout.getEngineState().getUser().getId(),
											ball.getX(), ball.getY(), ball.getXdir(), ball.getYdir())));
				}
			}.start();

		}

		else if (ball.getX() == crazyBreakout.getEngineState().getwScreen()) {
			ball.setXdir(-1);
			new Thread() {
				public void run() {
					HitEvent hitEvent = new HitEvent("ball hit", "", crazyBreakout.getEngineState().getUser().getId(),
							ball.getX(), ball.getY(), ball.getXdir(), ball.getYdir());
					crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

				}
			}.start();
		}

		else if (ball.getY() == 0) {
			ball.setYdir(1);
			new Thread() {
				public void run() {
					HitEvent hitEvent = new HitEvent("ball hit", "", crazyBreakout.getEngineState().getUser().getId(),
							ball.getX(), ball.getY(), ball.getXdir(), ball.getYdir());
					crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

				}
			}.start();
		} else if ((ball.getRect()).intersects(paddle.getRect())) {
			int paddleLPos = (int) paddle.getRect().getMinX();
			int ballLPos = (int) ball.getRect().getMinX();

			int first = paddleLPos + 8;
			int second = paddleLPos + 16;
			int third = paddleLPos + 24;
			int fourth = paddleLPos + 32;

			if (ballLPos < first) {
				ball.setXdir(-1);
				ball.setYdir(-1);
				new Thread() {
					public void run() {
						HitEvent hitEvent = new HitEvent("ball hit", "",
								crazyBreakout.getEngineState().getUser().getId(), ball.getX(), ball.getY(),
								ball.getXdir(), ball.getYdir());
						crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

					}
				}.start();
			}

			else if (ballLPos >= first && ballLPos < second) {
				ball.setXdir(-1);
				ball.setYdir(-1 * ball.getYdir());
				new Thread() {
					public void run() {
						HitEvent hitEvent = new HitEvent("ball hit", "",
								crazyBreakout.getEngineState().getUser().getId(), ball.getX(), ball.getY(),
								ball.getXdir(), ball.getYdir());
						crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

					}
				}.start();
			}

			else if (ballLPos >= second && ballLPos < third) {
				ball.setXdir(0);
				ball.setYdir(-1);
				new Thread() {
					public void run() {
						HitEvent hitEvent = new HitEvent("ball hit", "",
								crazyBreakout.getEngineState().getUser().getId(), ball.getX(), ball.getY(),
								ball.getXdir(), ball.getYdir());
						crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent).toString());

					}
				}.start();
			}

			else if (ballLPos >= third && ballLPos < fourth) {
				ball.setXdir(1);
				ball.setYdir(-1 * ball.getYdir());
				new Thread() {
					public void run() {
						HitEvent hitEvent = new HitEvent("ball hit", "",
								crazyBreakout.getEngineState().getUser().getId(), ball.getX(), ball.getY(),
								ball.getXdir(), ball.getYdir());
						crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

					}
				}.start();
			}

			else if (ballLPos > fourth) {
				ball.setXdir(1);
				ball.setYdir(-1);
				new Thread() {
					public void run() {
						HitEvent hitEvent = new HitEvent("ball hit", "",
								crazyBreakout.getEngineState().getUser().getId(), ball.getX(), ball.getY(),
								ball.getXdir(), ball.getYdir());
						crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

					}
				}.start();
			}
		}

		for (int i = 0; i < N_OF_BRICKS; i++) {
			final int j = i;
			if ((ball.getRect()).intersects(bricks[i].getRect())) {

				int ballLeft = (int) ball.getRect().getMinX();
				int ballHeight = (int) ball.getRect().getHeight();
				int ballWidth = (int) ball.getRect().getWidth();
				int ballTop = (int) ball.getRect().getMinY();

				Point pointRight = new Point(ballLeft + ballWidth + 1, ballTop);
				Point pointLeft = new Point(ballLeft - 1, ballTop);
				Point pointTop = new Point(ballLeft, ballTop - 1);
				Point pointBottom = new Point(ballLeft, ballTop + ballHeight + 1);

				if (bricks[i].isActive()) {
					if (bricks[i].getRect().contains(pointRight)) {
						ball.setXdir(-1);
					} else if (bricks[i].getRect().contains(pointLeft)) {
						ball.setXdir(1);
					}

					if (bricks[i].getRect().contains(pointTop)) {
						ball.setYdir(1);
					} else if (bricks[i].getRect().contains(pointBottom)) {
						// ball.setYdir(-1);
					}
					// bricks[i].setActive(false);
					// bricks[i].setX(2000000);
					new Thread() {
						public void run() {
							HitBlockEvent hitBlockEvent = new HitBlockEvent("hit block", "",
									crazyBreakout.getEngineState().getUser().getId(), bricks[j].getId(), ball.getX(),
									ball.getY(), ball.getXdir(), ball.getYdir());
							String var = gson.toJson(hitBlockEvent);
							crazyBreakout.getNetwork().sendEventBuffer(var);

							HitEvent hitEvent = new HitEvent("ball hit", "",
									crazyBreakout.getEngineState().getUser().getId(), ball.getX(), ball.getY(),
									ball.getXdir(), ball.getYdir());
							crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(hitEvent));

						};
					}.start();

				}
			}

		}
	}

	private Block[] searchBloks() {
		ArrayList<Sprite> blocks = new ArrayList<>();
		for (Sprite sprite : crazyBreakout.getStage().getGameObjects()) {
			if (sprite.getType() == 0) {
				blocks.add(sprite);
			}
		}
		return blocks.toArray(new Block[blocks.size()]);
	}

	private void initListeners(JPanel panel, final Bar gun) {
		gameFrame.getGamePane().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_RIGHT:
					gun.move(2);
					new Thread() {
						public void run() {
							HitEvent barMove = new HitEvent("bar move", "",
									crazyBreakout.getEngineState().getUser().getId(), gun.getX(), gun.getY(), -1, -1);
							crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(barMove));
						}
					}.start();
					break;
				case KeyEvent.VK_LEFT:
					gun.move(4);
					new Thread() {
						public void run() {
							HitEvent barMove = new HitEvent("bar move", "",
									crazyBreakout.getEngineState().getUser().getId(), gun.getX(), gun.getY(), -1, -1);
							crazyBreakout.getNetwork().sendEventBuffer(gson.toJson(barMove));
						}
					}.start();
					break;
				default:
					break;
				}
			}
		});
	}

	private Bar startBar(int userId) {
		if (userId != crazyBreakout.getEngineState().getUser().getId()) {
			Bar bar = new Bar(userId, 100, crazyBreakout.getEngineState().gethScreen() - 20, 0, 80, 20, 0,
					"src/images/paddle.png", 1) {
				@Override
				public void move(int type) {
					super.move(type);
				}
			};
			crazyBreakout.getStage().addSprite(bar);
			return bar;
		} else {
			Bar bar = new Bar(userId, 100, crazyBreakout.getEngineState().gethScreen() - 20, 0, 80, 20, 0,
					"src/images/bar1.png", 1) {
				@Override
				public void move(int type) {
					super.move(type);
				}

			};
			crazyBreakout.getStage().addSprite(bar);
			return bar;
		}
	}

	private void initNetwork() {
		crazyBreakout.getNetwork().openConnection();
		crazyBreakout.getNetwork().addObserver(new Observer() {
			@Override
			public void update(Observable obs, String event, Object message) {

			}
		});
	}

	private void setUsers(List<Double> users) {
		for (Double i : users) {
			if ((i != -1) && !(i == crazyBreakout.getEngineState().getUser().getId())) {
				starBall((i.intValue()));
				startBar((i.intValue()));
			}
		}
	}

	public CrazyBreackout getGame() {
		return crazyBreakout;
	}

	public void setGame(CrazyBreackout game) {
		this.crazyBreakout = game;
	}

	public GameFrame getGameFrame() {
		return gameFrame;
	}

	public void setGameFrame(GameFrame gameFrame) {
		this.gameFrame = gameFrame;
	}

	private void initLocalWorld() {
	}

}