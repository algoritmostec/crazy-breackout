package client.control;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import bohr.engine.options.ServerProtocol;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;
import bohr.engine.util.Util;
import client.view.menu.CreatePanel;
import client.view.menu.FinishPanel;
import client.view.menu.JoinPanel;
import client.view.menu.MenuPanel;
import client.view.menu.SetupPanel;
import client.view.menu.UserPanel;
import javax.swing.JButton;
import server.model.game.CrazyBreackout;
import server.model.game.options.GameProtocol;

public class MenuController {

	private CrazyBreackout crazyBreackout;
	private JFrame window;

	public MenuController() {
		crazyBreackout = new CrazyBreackout();
		window = new JFrame();
		initWindow();
		initMenu();
	}

	// Configura pantalla básica, vacía, donde se adhieren los paneles
	private void initWindow() {
		window.setVisible(true);
		window.setBounds(0, 0, 800, 600);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// Inicia la ventana principal que representa el menu y las acciones
	// de los botones
	private void initMenu() {
		final MenuPanel menuPanel = new MenuPanel("src/images/menu/fMenu.jpg");
		menuPanel.getbJoin().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				initJoinPanel();
			}
		});
		menuPanel.getbSetup().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				initSetupPanel();
			}
		});
		menuPanel.getbQuit().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		window.setContentPane(menuPanel);
	}

	// Inicia el panel para configuración
	private void initSetupPanel() {
		final SetupPanel setupPanel = new SetupPanel("src/images/menu/fMenu.jpg");
		// Carga los datos de configuración
		setupPanel.initFields(//
				crazyBreackout.getEngineState().getUsername(), //
				crazyBreackout.getEngineState().getPassword(), //
				crazyBreackout.getEngineState().getServerHost(), //
				crazyBreackout.getEngineState().getPort(), //
				crazyBreackout.getEngineState().getwScreen(), //
				crazyBreackout.getEngineState().gethScreen());

		// Actualiza los datos de configuración
		setupPanel.getbSave().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// !!!!!SE DEBEN VALIDAR LOS DATOS!!!!
				crazyBreackout.getEngineState().setPort(//
						Integer.valueOf(setupPanel.getTextPort().getText()));
				crazyBreackout.getEngineState().setServerHost(//
						setupPanel.getServerHost().getText());
				crazyBreackout.getEngineState().setUsername(//
						setupPanel.getTextUser().getText());
				crazyBreackout.getEngineState().setPassword(//
						new String(setupPanel.getTextPassword().getPassword()));
				//messageDialog(setupPanel, 7);
			}
		});
		goToMainMenuButton(setupPanel.getbBack());
		window.setContentPane(setupPanel);
	}

	// Inicial el panel para unirse al juego
	private void initJoinPanel() {
		final JoinPanel joinPanel = new JoinPanel("src/images/menu/fSetup.jpg");
		joinPanel.getbJoin().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				execJoin(joinPanel);
			}
		});
		goToMainMenuButton(joinPanel.getbBack());
		window.setContentPane(joinPanel);
	}

	private void execJoin(JoinPanel joinPanel) {
		String txtId = joinPanel.getTextJoin().getText();
		if (!Util.isInteger(txtId)) {
			// messageDialog(joinPanel, 1);
			return;
		}
		System.out.println("dab f fkljdkla");
		// Si no se ha conectado al server, no envía el request de entrada
		if (!crazyBreackout.getNetwork().isServerOn()) {
			//messageDialog(joinPanel, 0);
		} else {
			String getSession = ServerProtocol.GET_SESSION + " " + //
					crazyBreackout.getEngineState().getUser().getId();
			String joinSession = ServerProtocol.JOIN_GAME + " " + //
					crazyBreackout.getEngineState().getUser().getId() + " " + //
					crazyBreackout.getEngineState().getUsername() + " " + //
					crazyBreackout.getEngineState().getPassword();
			/*
			 * if ((game.getNetwork().sendEvent(//
			 * joinSession)).get("response").equals("error"))
			 * messageDialog(joinPanel, 3); else if
			 * (!(game.getNetwork().sendEvent(//
			 * getSession)).get("response").equals("ok"))
			 * messageDialog(joinPanel, 1); else initGame();
			 */
		}
		initGame();
	}

	private void initFinishPanel(boolean finish) {
		final FinishPanel finishPanel = new FinishPanel(finish);
		finishPanel.getbComeBack().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				initMenu();
			}
		});
		window.setContentPane(finishPanel);
	}

	private void initGame() {
		window.setVisible(false);
		final GameController gc = new GameController(crazyBreackout);
			
		/*
		 * gc.getGame().getNetwork().addObserver(new Observer() {
		 * 
		 * @Override public void update(Observable obs, String msg, Object obj)
		 * { if (msg.equals(GameProtocol.GAME_OVER_COMMAND)) {
		 * initFinishPanel(true); window.setVisible(true);
		 * gc.getGameFrame().dispatchEvent(new WindowEvent(// gc.getGameFrame(),
		 * WindowEvent.WINDOW_CLOSING)); } else if
		 * (msg.equals(GameProtocol.GAME_WIN_COMMAND)) { initFinishPanel(false);
		 * window.setVisible(true); gc.getGameFrame().dispatchEvent(new
		 * WindowEvent(// gc.getGameFrame(), WindowEvent.WINDOW_CLOSING)); } }
		 * });
		 */
		// window.setContentPane(gamePanel);
		gc.start();
	}

	private void messageDialog(JComponent comp, int type) {
		switch (type) {
		case 0:
			JOptionPane.showMessageDialog(comp, //
					"The game server is disconnected or there is a problem " + //
							"with the server�s host or port number.");
			break;
		case 1:
			JOptionPane.showMessageDialog(comp, "Invalid session ID.");
			break;
		case 2:
			JOptionPane.showMessageDialog(comp, "Error while creating session");
			break;
		case 3:
			JOptionPane.showMessageDialog(comp, "A user is already joined to this session " + //
					"or username or password are invalid");
			break;
		case 4:
			JOptionPane.showMessageDialog(comp, "There is another user with this username");
			break;
		case 5:
			JOptionPane.showMessageDialog(comp, "User created");
			break;
		case 6:
			JOptionPane.showMessageDialog(comp, "You need an account");
			break;
		case 7:
			JOptionPane.showMessageDialog(comp, "The configuration has been save succesfully");
			break;
		}
	}

	private void goToMainMenuButton(JButton btn) {
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				initMenu();
			}
		});
	}
}
