package client.view.menu;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

public class MenuPanel extends Fondo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblCrazyHuntDuck = new JLabel("Crazy Breakout");
	private JButton bJoin = new JButton("");
	private JButton bSetup = new JButton("");
	private JButton bQuit = new JButton("");

	public MenuPanel(String path) {
		super(path);
		setBounds(0, 0, 800, 600);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);

		lblCrazyHuntDuck.setFont(new Font("Segoe Print", Font.BOLD | Font.ITALIC, 70));
		lblCrazyHuntDuck.setBounds(12, 0, 770, 155);

		//bCreate.setBounds(518, 127, 200, 50);
		bJoin.setBounds(518, 127+(306-246), 200, 50);
		//bUser.setBounds(518, 246, 200, 50);
		bSetup.setBounds(518, 246, 200, 50);
		bQuit.setBounds(518, 306, 200, 50);

		bJoin.setIcon(new ImageIcon("src/images/menu/bJoin.png"));
		bSetup.setIcon(new ImageIcon("src/images/menu/bSetup.png"));
		bQuit.setIcon(new ImageIcon("src/images/menu/bQuit.png"));
		
		this.add(bJoin);
		this.add(bSetup);
		this.add(bQuit);
		this.add(lblCrazyHuntDuck);
	}

	public JLabel getLblCrazyHuntDuck() {
		return lblCrazyHuntDuck;
	}

	public void setLblCrazyHuntDuck(JLabel lblCrazyHuntDuck) {
		this.lblCrazyHuntDuck = lblCrazyHuntDuck;
	}


	public JButton getbJoin() {
		return bJoin;
	}

	public void setbJoin(JButton bJoin) {
		this.bJoin = bJoin;
	}

	public JButton getbSetup() {
		return bSetup;
	}

	public void setbSetup(JButton bSetup) {
		this.bSetup = bSetup;
	}

	public JButton getbQuit() {
		return bQuit;
	}

	public void setbQuit(JButton bQuit) {
		this.bQuit = bQuit;
	}
	

}
