package client.view.menu;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GameFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtMessage;
	private JPanel gamepane;
	private JTextArea txtChat;
	private JLabel score;
	private JLabel result;

	public GameFrame() {
		setSize(994, 500);

		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 0));
		contentPane.setForeground(new Color(0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		gamepane = new JPanel();
		gamepane.setBounds(10, 11, 750, 400);
		contentPane.add(gamepane);
		gamepane.setLayout(null);
		gamepane.setFocusable(true);

		txtChat = new JTextArea();
		txtChat.setEditable(false);
		txtChat.setFocusable(false);
		txtChat.setBounds(770, 50, 205, 300);
		contentPane.add(txtChat);

		txtMessage = new JTextField();
		txtMessage.setBounds(770, 373, 205, 40);
		contentPane.add(txtMessage);
		txtMessage.setColumns(10);

		JButton btnSendMessage = new JButton("Send");
		btnSendMessage.setBounds(886, 424, 89, 23);
		contentPane.add(btnSendMessage);
		
		score = new JLabel("SCORE:");
		result = new JLabel(0+"");
		score.setForeground(new Color(255, 255, 255));
		score.setFont(new Font("Segoe Print", Font.BOLD, 30));
		score.setBounds(770, 5, 205, 50);
		
		result.setForeground(new Color(255, 255, 255));
		result.setFont(new Font("Segoe Print", Font.BOLD, 30));
		result.setBounds(900, 5, 50, 50);
		
		contentPane.add(score);
		contentPane.add(result);
		
		
		this.setVisible(true);
		setResizable(false);

		// Se inhabilitan estos componentes porque por ahora
		// no poseen funcionalidad
		txtMessage.setFocusable(false);
		btnSendMessage.setFocusable(false);
	}
	public JPanel getGamePane() {
		return gamepane;
	}

	@Override
	public JPanel getContentPane() {
		return contentPane;
	}

	public JTextField getTxtMessage() {
		return txtMessage;
	}

	public JPanel getGamepane() {
		return gamepane;
	}

	public JTextArea getTxtChat() {
		return txtChat;
	}
	public JLabel getScore() {
		return score;
	}
	public void setScore(JLabel score) {
		this.score = score;
	}
	public JLabel getResult() {
		return result;
	}
	public void setResult(JLabel result) {
		this.result = result;
	}
	

	

}
