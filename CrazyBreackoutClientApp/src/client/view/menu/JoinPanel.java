package client.view.menu;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class JoinPanel extends Fondo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bBack = new JButton("");
	private JButton bJoin = new JButton("");
	private JLabel lblSessionId = new JLabel("JOIN TO ID: ");
	private JTextField textJoin = new JTextField();
	
	public JoinPanel(String path) {
		super(path);
		setBounds(0, 0, 800, 600);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);

		lblSessionId.setForeground(new Color(255, 255, 255));
		lblSessionId.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblSessionId.setBounds(40, 100, 770, 105);
		this.add(lblSessionId);

		textJoin.setBounds(300, 100 + 40, 300, 30);
		textJoin.setColumns(10);
		this.add(textJoin);

		bBack = new JButton("");
		bBack.setIcon(new ImageIcon("src/images/menu/bBack.png"));

		bBack.setBounds(518, 400, 200, 50);
		this.add(bBack);

		bJoin = new JButton("");
		bJoin.setIcon(new ImageIcon("src/images/menu/bJoin.png"));
		bJoin.setBounds(300, 400, 200, 50);
		this.add(bJoin);
	}

	public JButton getbBack() {
		return bBack;
	}

	public void setbBack(JButton bBack) {
		this.bBack = bBack;
	}

	public JButton getbJoin() {
		return bJoin;
	}

	public void setbJoin(JButton bJoin) {
		this.bJoin = bJoin;
	}

	public JLabel getLblSessionId() {
		return lblSessionId;
	}

	public void setLblSessionId(JLabel lblIp) {
		this.lblSessionId = lblIp;
	}

	public JTextField getTextJoin() {
		return textJoin;
	}

	public void setTextJoin(JTextField textJoin) {
		this.textJoin = textJoin;
	}

}
