package client.view.menu;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class UserPanel extends Fondo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bBack = new JButton("");
	private JButton bUser = new JButton("");
	private JLabel lblUser = new JLabel("USER NAME: ");
	private JLabel lblPassword = new JLabel("PASSWORD: ");
	private JPasswordField textPassword = new JPasswordField();
	private JTextField textUser = new JTextField();

	public UserPanel(String path) {
		super(path);
		setBounds(0, 0, 800, 600);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);

		lblUser.setForeground(new Color(255, 255, 255));
		lblUser.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblUser.setBounds(40, 100, 770, 155);
		this.add(lblUser);

		textUser.setBounds(300, 160, 300, 30);
		this.add(textUser);
		textUser.setColumns(10);

		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("Segoe Print", Font.BOLD, 30));
		lblPassword.setBounds(40, 150, 770, 155);
		this.add(lblPassword);

		textPassword.setBounds(300, 210, 300, 30);
		this.add(textPassword);
		textPassword.setColumns(10);

		bBack.setIcon(new ImageIcon("src/images/menu/bBack.png"));
		bBack.setBounds(518, 400, 200, 50);
		this.add(bBack);

		bUser = new JButton("");
		bUser.setIcon(new ImageIcon("src/images/menu/bUser.png"));
		bUser.setBounds(300, 400, 200, 50);
		this.add(bUser);
	}

	public JButton getbBack() {
		return bBack;
	}

	public void setbBack(JButton bBack) {
		this.bBack = bBack;
	}

	public JButton getbUser() {
		return bUser;
	}

	public void setbUser(JButton bUser) {
		this.bUser = bUser;
	}

	public JLabel getLblUser() {
		return lblUser;
	}

	public void setLblUser(JLabel lblUser) {
		this.lblUser = lblUser;
	}

	public JLabel getLblPassword() {
		return lblPassword;
	}

	public void setLblPassword(JLabel lblPassword) {
		this.lblPassword = lblPassword;
	}

	public JPasswordField getTextPassword() {
		return textPassword;
	}

	public void setTextPassword(JPasswordField textPassword) {
		this.textPassword = textPassword;
	}

	public JTextField getTextUser() {
		return textUser;
	}

	public void setTextUser(JTextField textUser) {
		this.textUser = textUser;
	}

}
