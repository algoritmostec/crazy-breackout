package game.model.util;

import java.util.ArrayList;

import bohr.engine.admin.User;
import game.model.entities.others.GameResource;
import game.model.entities.clan.Clan;

/**
 * Created by randall on 14/09/2015.
 * @author Randall Alfaro
 * Clase encargada de las busquedas de datos en las diferentes listas.
 * Las listas utilizadas son como la lista de User,Clanes,Recursos
 */
public class Search {
    /**
     * Buscar un usuario en la lista de usuarios por ID
     * @param idPlayer ID del jugadore que se desea buscar
     * @param users La lista en la que se desea buscar
     * @return null en caso de que no se encuentre y idPlayer en caso de que si
     */
    public static User getUserById(int idPlayer, ArrayList<User> users) {
        for (User u : users) {
            if (u.getId() == idPlayer) {
                return u;
            }
        }
        return null;
    }

    /**
     *  Buscar un clan en la lista de Clanes por ID
     * @param idClan ID del clan que se desea buscar
     * @param clans Lista de clanes
     * @return null en caso de que no se encuentre y idClan en caso de que si
     */
    public static Clan getClanbyId(int idClan, ArrayList<Clan> clans) {
        for (Clan u : clans) {
            if (u.getId() == idClan) {
                return u;
            }
        }
        return null;
    }

    /**
     *  Buscar un clan en la lista de clanes por el nombre
     * @param clanName Nombre del clan
     * @param clans lista de clanes
     * @return null en caso de que no se encuentre y clanName en caso de que si
     */
    public static Clan getClanByName(String clanName, ArrayList<Clan> clans) {
        for (Clan u : clans) {
            if (u.getClanName().equals(clanName)) {
                return u;
            }
        }
        return null;
    }

    /**
     *  Buscar un recurso en la lista de recursos
     * @param idClan ID del clan que se desea buscar
     * @param resources lista de Recursos
     * @return null en caso de que no se encuentre y idClan en caso de que si
     */
    public static GameResource getGameResourceById(int idClan, ArrayList<GameResource> resources) {
        for (GameResource u : resources) {
            if (u.getId() == idClan) {
                return u;
            }
        }
        return null;
    }


}
