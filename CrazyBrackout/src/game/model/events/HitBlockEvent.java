package game.model.events;

public class HitBlockEvent extends HitEvent {
	private int blockId;
	public HitBlockEvent(String event, String type, int userId, int blockId, float x, float y, int xDir, int yDir) {
		super(event, type, userId,x,y,xDir,yDir);
		this.blockId=blockId;
	}
	public int getBlockId() {
		return blockId;
	}
	public void setBlockId(int blockId) {
		this.blockId = blockId;
	}
}
