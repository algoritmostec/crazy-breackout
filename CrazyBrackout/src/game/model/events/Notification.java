package game.model.events;


public class Notification {
	private String event;
	private String type;
	public Notification(String event, String type){
		this.event=event;
		this.type=type;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
