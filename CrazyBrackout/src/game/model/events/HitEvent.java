package game.model.events;

public class HitEvent extends Event {
	private float x;
	private float y;
	private int xDir;
	private int yDir;

	public HitEvent(String event, String type, int userId, float x, float y, int xDir, int yDir) {
		super(event, type, userId);
		this.x = x;
		this.y = y;
		this.xDir = xDir;
		this.yDir = yDir;
	}

	public float getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getxDir() {
		return xDir;
	}

	public void setxDir(int xDir) {
		this.xDir = xDir;
	}

	public int getyDir() {
		return yDir;
	}

	public void setyDir(int yDir) {
		this.yDir = yDir;
	}
	
}
