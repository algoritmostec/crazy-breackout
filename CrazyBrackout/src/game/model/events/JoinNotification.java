package game.model.events;

public class JoinNotification extends Notification {
	private int idUser;

	public JoinNotification(String event, String type, int idUser) {
		super(event, type);
		this.idUser = idUser;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

}
