package game.model.net;

import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;

/**
 * Clase encargada de las solicitudes para la obtencion de recursos
 *@author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameResourceRequest {
    private GameResource gameResource;
    private int user;
    private GeoPoint geoPoint;

    /**
     * Constructor
     * @param gameResource Recurso del juego
     * @param user ID del usuario
     * @param geoPoint la localizacion del recurso
     */
    public GameResourceRequest(GameResource gameResource, int user, GeoPoint geoPoint) {
        this.gameResource = gameResource;
        this.user = user;
        this.geoPoint = geoPoint;
    }

    public GameResourceRequest() {
        this(new GameResource(), -1, new GeoPoint());
    }

    public GameResource getGameResource() {
        return gameResource;
    }

    public void setGameResource(GameResource gameResource) {
        this.gameResource = gameResource;
    }

    public int getUserId() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }
}
