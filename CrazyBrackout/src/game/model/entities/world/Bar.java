package game.model.entities.world;

import bohr.engine.stage.Sprite;

public class Bar extends Sprite {

    public Bar(int id, int x, int y, int z, int width,
            int height, int angle, String asset, int type) {
        super(id, x, y, z, width, height, angle, asset, type);

    }

    @Override
    public void step(int dt) {
        
    }

    public void move(int type) {
        switch (type) {
            case 1:
                move(0, -10, 0);
                break;
            case 2:
                this.move(10, 0, 0);
                break;
            case 3:
                this.move(0, 10, 0);
                break;
            default:
                this.move(-10, 0, 0);
                break;
        }
    }
    // super.move(dx, dy, dz);
/*
     public JSONObject toJSON() {
     JSONObject pJson = new JSONObject();
     try {
     pJson.put("id", id);
     pJson.put("x", x);
     pJson.put("y", y);
     pJson.put("type", type);
     pJson.put("height", height);
     pJson.put("width", width);
     } catch (JSONException e) {
     e.printStackTrace();
     }
     return pJson;
     }*/
}
