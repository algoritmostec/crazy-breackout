package game.model.entities.world;

import bohr.engine.stage.Sprite;

public class Block extends Sprite {
	private int content;
	private boolean active;
	private int posX;
	private int posY;

	public Block(int id, int type, float x, float y, float z, int width, int height, int angle, String asset) {
		super(id, x, y, z, width, height, angle, asset, type);
		this.id = id;
		this.content = type;
		active = true;
		posY = 0;
		posX = 0;
	}

	
	public Block() {
		super();
		this.id = -1;
		this.content = -1;
		active = false;
		posY = 0;
		posX = 0;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return content;
	}

	public void setType(int type) {
		this.content = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
