package game.model.entities.world;

import java.util.ArrayList;
import java.util.List;

public class World {
	private List<Block> blocks;
	private int size;

	public int getSize() {
		return size;
	}

	public void setSize(int sizeX) {
		this.size = sizeX;
	}

	public World(int size) {
		this.size = size;
		this.blocks = new ArrayList();
	}

	public World() {
		size = 40;
		blocks = new ArrayList();
	}

	public List<Block> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<Block> blocks) {
		this.blocks = blocks;
	}

}
