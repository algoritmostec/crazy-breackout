package game.model.entities.world;

import bohr.engine.stage.Sprite;

public class Ball extends Sprite {

    private int xdir;
    private int ydir;
    private boolean active;
    
	public Ball(int id, int x, int y, int z, int width, int height, int angle, String asset, int type) {
		super(id, x, y, z, width, height, angle, asset, type);
		xdir=1;
		ydir=-1;
		active=true;
		
	}

	public void step(int dt) {
		x += xdir;
		y += ydir;
	}

	public int getXdir() {
		return xdir;
	}

	public void setXdir(int xdir) {
		this.xdir = xdir;
	}

	public int getYdir() {
		return ydir;
	}

	public void setYdir(int ydir) {
		this.ydir = ydir;
	}
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	

}
