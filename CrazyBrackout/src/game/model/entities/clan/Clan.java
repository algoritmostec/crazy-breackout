package game.model.entities.clan;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;

import bohr.engine.util.Util;
import bohr.engine.util.observable.Observable;
import game.model.entities.others.AttackResourse;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.entities.others.Message;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que almacena los datos de un clan
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Clan extends Observable  {
    private int id;
    private int type;
    private String clanName;
    private int owner;
    private GeoPoint geoPoint;
    private ArrayList<GameResource> resources;
    private ArrayList<ClanMember> members;
    private ArrayList<AttackResourse> attackResourses;
    private ArrayList<Message> messages;

    /**
     * Constructor de la clase
     *
     * @param id       id del clan
     * @param type     tipo del clan
     * @param clanName nombre del clan
     * @param owner    creador del clan
     * @param geoPoint localizacion de la reliquia del clan
     */
    public Clan(int id, int type, String clanName, int owner, GeoPoint geoPoint) {
        this.id = id;
        this.type = type;
        this.clanName = clanName;
        this.owner = owner;
        this.geoPoint = geoPoint;
        resources = new ArrayList<>();
        members = new ArrayList<>();
        messages = new ArrayList<>();
        attackResourses = new ArrayList<>();

    }

    /**
     * Constructor auxiliar  de la clase
     */
    public Clan() {
        this.id = -1;
        this.type = 1;
        this.clanName = "";
        this.owner = -1;
        this.geoPoint = new GeoPoint();
        resources = new ArrayList<>();
        members = new ArrayList<>();
        messages = new ArrayList<>();
        attackResourses = new ArrayList<>();

    }

    public ArrayList<AttackResourse> getAttackResourses() {
        return attackResourses;
    }

    public void setAttackResourses(ArrayList<AttackResourse> attackResourses) {
        this.attackResourses = attackResourses;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public ArrayList<ClanMember> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<ClanMember> members) {
        this.members = members;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setClanName(String clanName) {
        this.clanName = clanName;
    }

    public void setResources(ArrayList<GameResource> resources) {
        this.resources = resources;
    }

    public ArrayList<GameResource> getResources() {
        return resources;
    }

    public void setType(int _type) {
        this.type = _type;
    }

    public void setClanname(String clanname) {
        this.clanName = clanname;
    }

    public void setOwner(int _owner) {
        this.owner = _owner;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public int getType() {
        return type;
    }

    public String getClanName() {
        return clanName;
    }

    public int getOwner() {
        return owner;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    /**
     * Metodo para actualizar los recursos del clan
     *
     * @param type     tipo de recurso
     * @param quantity cantidad que se agrega de un recurso
     */
    public void updateResources(int type, int quantity) {
        boolean created = false;
        for (GameResource gr : resources) {
            if (gr.getType() == type) {
                created = true;
                gr.setQuantity(gr.getQuantity() + quantity);
            }
        }
        if (!created) {
            resources.add(new GameResource(type, quantity));
        }
        updateClan();
    }

    /**
     * Metodo para saber la cantidad de recursos segun el tipo
     *
     * @param type tipo de recurso
     * @return cantidad del recurso buscado
     */
    public int quantityResourceByType(int type) {
        for (GameResource gr : resources) {
            if (gr.getType() == type) {
                return gr.getQuantity();
            }

        }
        return 0;
    }


    /**
     * Metodo para hacer un clan como JSON
     */
    private void updateClan() {
        DataResponse<Clan> update = new DataResponse<>(this);
        update.setResponse(CommunicationProtocol.CLAN_UPDATE);
        notifyObservers(new Gson().toJson(update));
    }
}
