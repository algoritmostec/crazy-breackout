package game.model.entities.clan;

import java.io.Serializable;

/**
 * Clase que almacena los datos de un miembro del clan
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ClanMember {
    private int idUser;
    private double resourcesCount;
    private int range;

    /**
     * Constructor de la clase
     *
     * @param idUser         id del usuario
     * @param resourcesCount cantidad de recursos del miembro aportado al clan
     * @param range          rango en el clan
     */
    public ClanMember(int idUser, double resourcesCount, int range) {
        this.idUser = idUser;
        this.resourcesCount = resourcesCount;
        this.range = range;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public ClanMember() {
        this(-1, 0, -1);
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public double getResourcesCount() {
        return resourcesCount;
    }

    public void setResourcesCount(double resourcesCount) {
        this.resourcesCount = resourcesCount;
    }

}
