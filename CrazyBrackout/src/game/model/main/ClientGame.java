package game.model.main;

import java.io.Serializable;
import java.util.ArrayList;

import bohr.engine.GameEngine;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;
import game.model.entities.world.Block;

/**
 * Clase de cliente del juego encargada de los recursos y los clanes inicia el
 * juego
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class ClientGame extends GameEngine implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<Block> blocks;

	public ClientGame() {
		super();
		blocks = new ArrayList<>();
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public ArrayList<Block> getResources() {
		return blocks;
	}

	public void setResources(ArrayList<Block> resources) {
		this.blocks = resources;
	}
	/*
	 * public void event(Object sender, String event, JSONObject obj) { switch
	 * (event) { case ServerProtocol.MESSAGES_COMMAND: notifyObservers(event,
	 * obj.toString()); break; case ServerProtocol.MESSAGE_COMMAND:
	 * notifyObservers(event, obj.toString()); break; default: break; } }
	 */

}
