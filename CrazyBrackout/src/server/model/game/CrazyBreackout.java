package server.model.game;

import bohr.engine.GameEngine;
import bohr.engine.options.ServerProtocol;
import bohr.engine.stage.Sprite;
import game.model.entities.world.World;
import game.model.entities.world.Ball;
import game.model.entities.world.Block;
import game.model.main.ClientGame;

import java.util.Iterator;
import java.util.Random;
import server.model.game.options.GameProtocol;

public class CrazyBreackout extends ClientGame {
	private World world;

	public CrazyBreackout() {
		super();
	}

	public void setWorld(World world) {
		this.world = world;
		initWorld();
	}

	public void event(Object sender, String event) {
		float percScreenY = this.getEngineState().gethScreen() / 100;
		float percScreenX = this.getEngineState().getwScreen() / 100;
		/*
		 * switch (event) { case GameProtocol.NEW_DUCK_COMMAND:
		 * this.getStage().addSprite(new Duck(obj)); break; case
		 * GameProtocol.NEW_DUCK_POINT_COMMAND: addDuckPoint(obj.getInt("id"),
		 * percScreenY * obj.getInt("x"), // percScreenX * obj.getInt("y"));
		 * break; case GameProtocol.DEL_DUCK_COMMAND: delDuck(obj.getInt("id"));
		 * break; case ServerProtocol.MESSAGES_COMMAND: notifyObservers(event,
		 * obj.toString()); break; case ServerProtocol.MESSAGE_COMMAND:
		 * notifyObservers(event, obj.toString()); break; case
		 * GameProtocol.GAME_OVER_COMMAND:
		 * this.getGameSetup().setFinished(true); break; default: break; }
		 */
	}

	private void delDuck(int id) {
		Sprite s = null;
		if ((s = getGameObject(id)) != null) {
			// this.getStage().getGameObjects().remove(s);
		}
	}

	private Sprite getGameObject(int id) {
		for (Sprite s : this.getStage().getGameObjects()) {
			if (s.getId() == id) {
				return s;
			}
		}
		return null;
	}

	private void initWorld() {
		Random rand = new Random();
		int id = 1;
		int spaceColumn = 30;
		int spaceRow = 4;
		int posXStart = 20;
		int posYStart = 40;
		// int sizeX = getView().getGamePanel().getWidth() / 10;
		int sizeX = 45;
		int sizeY = 30;
		float angle = 1;
		Iterator<Block> iterator = world.getBlocks().iterator();
		while (iterator.hasNext()) {
			Block block = iterator.next();
			block.setX(posXStart + block.getPosX() * (sizeX + spaceColumn));
			block.setY(posYStart + block.getPosY() * (sizeY + spaceColumn));
			block.setAsset("src/images/brick.png");
			block.setWidth(sizeX);
			block.setHeight(sizeY);
			block.setActive(true);
			block.setType(0);
			this.getStage().addSprite(block);
		}
	}

	/*
	 * private String getAsset(int type) { switch (type) { case 1: return
	 * "src/images/bar1.png"; case 2: return "src/images/pSalvaje.gif"; case 3:
	 * return "src/images/pGanzoHawaii.png"; default: return
	 * "src/images/pColorado.png"; } }
	 */
}
