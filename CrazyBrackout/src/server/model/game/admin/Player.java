package server.model.game.admin;

import java.net.Socket;

public class Player {
	private int ID;
	private Socket socket;
	private String username;
	private String password;

	public Player(int iD, Socket socket, String username, String password) {
		ID = iD;
		this.socket = socket;
		this.username = username;
		this.password = password;
	}

	public int getID() {
		return ID;
	}

	public Socket getSocket() {
		return socket;
	}
        
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
