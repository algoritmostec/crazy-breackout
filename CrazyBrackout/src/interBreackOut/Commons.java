package interBreackOut;


public interface Commons {
    
    public static final int WIDTH = 600;
    public static final int HEIGTH = 800;
    public static final int BOTTOM_EDGE = 790;
    public static final int N_OF_BRICKS = 30;
    public static final int INIT_PADDLE_X = 200;
    public static final int INIT_PADDLE_Y = 680;
    public static final int INIT_BALL_X = 230;
    public static final int INIT_BALL_Y = 600;    
    public static final int DELAY = 1000;
    public static final int PERIOD = 10;
}