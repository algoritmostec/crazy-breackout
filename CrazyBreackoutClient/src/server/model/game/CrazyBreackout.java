package server.model.game;

import bohr.engine.GameEngine;
import bohr.engine.options.ServerProtocol;
import bohr.engine.stage.Sprite;
import java.util.Random;
import server.model.game.options.GameProtocol;
import server.model.game.sprites.Block;

public class CrazyBreackout extends GameEngine {

    public CrazyBreackout() {
        super();
        initializeLocalWorld();
    }

    public void event(Object sender, String event) {
        float percScreenY = this.getEngineState().gethScreen() / 100;
        float percScreenX = this.getEngineState().getwScreen() / 100;
        /*switch (event) {
         case GameProtocol.NEW_DUCK_COMMAND:
         this.getStage().addSprite(new Duck(obj));
         break;
         case GameProtocol.NEW_DUCK_POINT_COMMAND:
         addDuckPoint(obj.getInt("id"), percScreenY * obj.getInt("x"), //
         percScreenX * obj.getInt("y"));
         break;
         case GameProtocol.DEL_DUCK_COMMAND:
         delDuck(obj.getInt("id"));
         break;
         case ServerProtocol.MESSAGES_COMMAND:
         notifyObservers(event, obj.toString());
         break;
         case ServerProtocol.MESSAGE_COMMAND:
         notifyObservers(event, obj.toString());
         break;
         case GameProtocol.GAME_OVER_COMMAND:
         this.getGameSetup().setFinished(true);
         break;
         default:
         break;
         }*/
    }

    private void delDuck(int id) {
        Sprite s = null;
        if ((s = getGameObject(id)) != null) {
            //this.getStage().getGameObjects().remove(s);
        }
    }

    private Sprite getGameObject(int id) {
        for (Sprite s : this.getStage().getGameObjects()) {
            if (s.getId() == id) {
                return s;
            }
        }
        return null;
    }

    private void initializeLocalWorld() {
        Random rand = new Random();
        int id = 1;
        int spaceColumn = 2;
        int spaceRow = 2;
        int posXStart = 20;
        int posYStart = 40;
        //int sizeX = getView().getGamePanel().getWidth() / 10;
        int sizeX = 45;
        int sizeY = 30;
        float angle = 1;
        for (float x = 0; x < 15; x++) {
            for (float y = 0; y < 5; y++) {
                int type = rand.nextInt(3) + 1;
                this.getStage().addSprite(
                        new Block(id, posXStart + x * (sizeX + spaceColumn),
                                posYStart + y * (sizeY + spaceRow), angle,
                                sizeX, sizeY, 1,
                                getAsset(type),
                                type, 0));
                id++;
            }
        }
        //this.getStage().addSprite(new Duck());
        /*int id, float x, float y, float z, int width, int height,
         int angle, String asset, int type, int strength*/
    }

    private String getAsset(int type) {
        switch (type) {
            case 1:
                return "src/images/bar1.png";
            case 2:
                return "src/images/pSalvaje.gif";
            case 3:
                return "src/images/pGanzoHawaii.png";
            default:
                return "src/images/pColorado.png";
        }
    }
}
