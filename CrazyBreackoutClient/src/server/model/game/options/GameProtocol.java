package server.model.game.options;

public class GameProtocol {
	// REGEX
	// final private static String SPACES = "[x0Bf]*";
	// final private static String ALFA_NUM = "\\{alnum\\}+";
	//final private static String ALFA_NUM = "[a-zA-Z0-9]+";
	final private static String NUM = "[0-9]+";
	//final private static String TWO_ALFA_NUM = "(" + ALFA_NUM + ") (" + ALFA_NUM + ")";

	// GAME EVENTS

	final public static String GAME_OVER_COMMAND = "game over";
	final public static String NEW_DUCK_COMMAND = "new duck";
	final public static String DEL_DUCK_COMMAND = "delete duck";
	final public static String DEL_DUCK_REGEX = DEL_DUCK_COMMAND + " (" + NUM + ")" + " (" + NUM + ")";
	final public static String NEW_DUCK_POINT_COMMAND = "new duck point";
	final public static String GUN_MOVE_COMMAND = "gun move";
	final public static String GUN_MOVE_REGEX = GUN_MOVE_COMMAND + " (" + NUM + ")" + " (" + NUM + ")";
	final public static String UPDATE_SCORE_COMMAND = "update score"; 	
	final public static String GAME_WIN_COMMAND = "win"; 

	/*
	 * final public static String CREATE_USER_REGEX = CREATE_USER + "\\(" +
	 * ALFA_NUM + "\\) " + SPACES + "\\(" + ALFA_NUM + "\\)";
	 */

	/*
	 * //SERVER CONECTION HELLO("hello"), // USER OPTIONS // CRUD CREATE_USER(
	 * "create user"), UPDATE_USER("update user"), DELETE_USER("delete user"),
	 * GET_USERS( "get users"), GET_USER("get user"),
	 * 
	 * // *MESSAGES SEND_MESSAGE_USER(""), SEND_MESSAGE_USERS(""),
	 * 
	 * // SESSION OPTIONS CREATE_SESSION("create session"), UPDATE_SESSION(""),
	 * DELETE_SESSION(""), (""), GET_SESSION(""),
	 * 
	 * // GAME OPTIONS INIT_GAME(""),
	 * 
	 * // ADMINISTATION OPTIONS EXIT("");
	 * 
	 * 
	 * public final String name;
	 * 
	 * private ProtocolType(String s) { name = s; }
	 * 
	 * public boolean equalsName(String otherName) { return (otherName == null)
	 * ? false : name.equals(otherName); }
	 * 
	 * public String toString() { return this.name; }
	 */
}