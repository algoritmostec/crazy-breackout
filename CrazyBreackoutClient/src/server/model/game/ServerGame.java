package server.model.game;

import java.util.ArrayList;

import bohr.engine.GameEngine;
import bohr.engine.game.GameState;
import server.model.game.options.GameProtocol;
import server.model.game.sprites.Block;
import server.model.game.sprites.Bar;
import server.model.game.thread.DuckThread;
import server.model.game.thread.ServerGameLoop;

public class ServerGame extends GameEngine {

    private ServerGameLoop main_loop;
    private ArrayList<Block> ducks;
    private Bar gun;

    public ServerGame() {
        main_loop = new ServerGameLoop(this);
        gun = new Bar(this.getGameSetup().getLastId(), 0, 0, 0, 80, 80, 0, "src/images/gun.png", 1);
        ducks = new ArrayList<>();
    }

    public void addDuck(int type) {
        int startX = (int) Math.floor(Math.random() * 100);
        int startY = 0;
        int height = 10;
        int width = 10;
        Block duck = null;
        this.getGameSetup().incID();
        int lastId = this.getGameSetup().getLastId();

        if (type <= this.getGameSetup().getLevel()) {
            duck = new Block(lastId, startX, startY, 0, height, width, 0, "image", 4, 1);
        } else if (type <= this.getGameSetup().getLevel() + 1) {
            duck = new Block(lastId, startX, startY, 0, height, width, 0, "image", 1, 30);
        } else if (type <= this.getGameSetup().getLevel() + 4) {
            duck = new Block(lastId, startX, startY, 0, height, width, 0, "image", 5, 5);
        } else if (type <= this.getGameSetup().getLevel() + 9) {
            duck = new Block(lastId, startX, startY, 0, height, width, 0, "image", 3, 2);
        } else {
            duck = new Block(lastId, startX, startY, 0, height, width, 0, "image", 2, 1);
        }
		 //System.out.println(GameProtocol.NEW_DUCK_COMMAND + duck.toJSON());
        //this.notifyEvent(GameProtocol.NEW_DUCK_COMMAND, duck.toJSON());
        ducks.add(duck);
        new DuckThread(this, duck).start();
    }

    public void gameOver() {
        for (Block d : ducks) {
            d.setAlive(false);
        }
        //this.notifyEvent(GameProtocol.GAME_OVER_COMMAND, new JSONObject());
        this.getGameSetup().setFinished(true);
    }

    //falta completar

    public void winner() {
        //this.notifyEvent(GameProtocol.GAME_WIN_COMMAND, new JSONObject());
        this.getGameSetup().setFinished(true);
    }

    public boolean kill(int id) {
        for (Block d : ducks) {
            if (d.getId() == id) {
                d.setStrength(d.getStrength() - 1);
                if (d.getStrength() <= 0) {
                    switch (d.getType()) {
                        case 1:
                            this.getGameSetup().setPoints(this.getGameSetup().getPoints() + 5);
                            break;
                        case 2:
                            this.getGameSetup().setPoints(this.getGameSetup().getPoints() + 1);
                            break;
                        case 3:
                            this.getGameSetup().setPoints(this.getGameSetup().getPoints() + 3);
                            break;
                        case 4:
                            this.getGameSetup().setPoints(this.getGameSetup().getPoints() + 5);
                        default:
                            this.getGameSetup().setPoints(this.getGameSetup().getPoints() + 2);
                    }
                    //notifyUpdateScore();
                    return true;
                }

            }
        }
        return false;
    }
    /*
     private void notifyUpdateScore() {
     JSONObject response = new JSONObject(); 
     try {
     response.put("score",getGameSetup().getPoints());
     notifyEvent(GameProtocol.UPDATE_SCORE_COMMAND, response );
     } catch (JSONException e) {
     e.printStackTrace();
     } 
		
     }

     public JSONArray toJSON() {
     JSONArray pJson = new JSONArray();
     for (Duck d : ducks) {
     if (d.getAlive()) {
     pJson.put(d.toJSON());
     }
     }
     return pJson;
     }
     */

    public Bar getGun() {
        return gun;
    }

    public ArrayList<Block> getDucks() {
        return ducks;
    }

    public Block getDuck(int id) {
        for (Block obj : ducks) {
            if (obj.getId() == id) {
                return obj;
            }
        }
        return null;
    }

    public void setDucks(ArrayList<Block> ducks) {
        this.ducks = ducks;
    }

    public ServerGameLoop getMain_loop() {
        return main_loop;
    }

    public GameState getGameState() {
        return this.getGameSetup();
    }
}
