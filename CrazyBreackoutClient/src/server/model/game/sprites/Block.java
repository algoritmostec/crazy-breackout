package server.model.game.sprites;

import java.util.ArrayList;
import java.util.List;
import bohr.engine.stage.Sprite;

public class Block extends Sprite {

    private boolean alive;
    private int strength;
    private double aRad;
    private double angle;

    public Block(int id, float x, float y, float z, int width, int height,
            int angle, String asset, int type, int strength) {
        super(id, x, y, z, width, height, angle, asset, type);
        this.strength = strength;
        alive = true;
    }


    /*public Duck(JSONObject jsonDuck) throws JSONException {
     this(jsonDuck.getInt("id"), jsonDuck.getInt("x") * 5, //
     jsonDuck.getInt("y") * 8, 0, 50, 50, 0, //
     jsonDuck.getString("asset"), 1, 1);
     }*/
    @Override
    public void step(int dt) {
    }

    public void printActual() {
        System.out.println("X: " + x + " Y: " + y);
    }

    public void getNextPoints() {
        x = (int) Math.floor(Math.random() * 100);
        y = (int) Math.floor(Math.random() * 100);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public boolean getAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
    /*
     public JSONObject toJSON() {
     JSONObject pJson = new JSONObject();
     try {
     pJson.put("id", id);
     pJson.put("x", x);
     pJson.put("y", y);
     pJson.put("type", type);
     pJson.put("height", height);
     pJson.put("width", width);
     pJson.put("alive", alive);
     pJson.put("strength", strength);
     pJson.put("asset", asset);
     } catch (JSONException e) {
     e.printStackTrace();
     }
     return pJson;
     }*/
}
