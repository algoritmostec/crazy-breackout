package server.model.game.thread;

import server.model.game.ServerGame;
import server.model.game.options.GameProtocol;
import server.model.game.sprites.Block;

public class DuckThread extends Thread {
	private int seconds = 500;
	private Block duck;
	private ServerGame game;

	public DuckThread(ServerGame game, Block duck) {
		this.game = game;
		this.duck = duck;
	}

	@Override
	public void run() {
		while (duck.getAlive()) {
			duck.getNextPoints();
			//game.notifyEvent(GameProtocol.NEW_DUCK_POINT_COMMAND, duck.toJSON());
			sleep();
		}
		try {
			join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void sleep() {
		try {
			super.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
