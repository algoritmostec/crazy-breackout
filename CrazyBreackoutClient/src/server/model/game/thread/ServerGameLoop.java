package server.model.game.thread;

import java.util.Random;

import bohr.engine.game.GameState;
import server.model.game.ServerGame;

public class ServerGameLoop extends Thread {
	private ServerGame game;
	private GameState state;
	private int timer;
	private int add;

	public ServerGameLoop(ServerGame game) {
		this.game = game;
		add = 10;
		timer = 0;
		state = game.getGameState();
	}

	public void run() {
		while (!state.isFinished()) {
			if (game.getDucks().size() > 19) {
				game.gameOver();
				return;
			}
			if (state.getLevel() == 21) {
				game.winner();
				return;
			}
			if (state.getSeconds() % 90 == 0) {
				state.incLevel();
				if (add > 1) {
					add--;
				}
			}
			if (timer % add == 0 || game.getDucks().size() == 0) {
				game.addDuck(new Random().nextInt(100));
				timer = 0;
			}
			timer++;
			sleep();
		}
	}

	private void sleep() {
		try {
			super.sleep(1000);
			state.incSeconds();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
