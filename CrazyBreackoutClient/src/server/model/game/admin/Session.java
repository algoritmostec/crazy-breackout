package server.model.game.admin;

import java.util.Date;

import server.model.game.ServerGame;

public class Session {
	private int id;
	private Date date;
	private ServerGame game;
	private Player player;

	public Session(Date date, ServerGame game, Player player, int id) {
		this.date = date;
		this.game = game;
		this.player = player;
		this.id = id;
	}

	public Session(Date date, int id) {
		this.date = date;
		this.game = new ServerGame();
		this.player = new Player(-1, null, "", "");
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ServerGame getGame() {
		return game;
	}

	public void setGame(ServerGame game) {
		this.game = game;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
/*
	public JSONObject toJSON() {
		JSONObject sJson = new JSONObject();
		try {
			sJson.put("date_created", date.toString());
			sJson.put("id", id);
			sJson.put("player_id", player.getID());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return sJson;
	}*/
}
