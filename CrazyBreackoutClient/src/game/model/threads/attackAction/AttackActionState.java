package game.model.threads.attackAction;

/**
 * Created by ijtj03 on 29/09/15.
 */
public class AttackActionState {
    private int time;
    private int idClanA;
    private int idClanB;
    private int seconds;

    public AttackActionState() {
        this.time = 600;
        this.idClanA = -1;
        this.idClanB = -1;
        this.seconds = 0;
    }
    public AttackActionState(int time, int idClanA, int idClanB, int seconds) {
        this.time = time;
        this.idClanA = idClanA;
        this.idClanB = idClanB;
        this.seconds = seconds;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getIdClanA() {
        return idClanA;
    }

    public void setIdClanA(int idClanA) {
        this.idClanA = idClanA;
    }

    public int getIdClanB() {
        return idClanB;
    }

    public void setIdClanB(int idClanB) {
        this.idClanB = idClanB;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
