package game.model.entities.others;

/**
 * Clase encargada de la cantidad de usuarios en zona de recoleccion de recursos
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class MostQuantity {
    private int idClan;
    private int quantity;

    /**
     * Constructor
     * @param idClan ID del clan
     * @param quantity Cantidad de miembros de ese clan
     */
    public MostQuantity(int idClan, int quantity) {
        this.idClan = idClan;
        this.quantity = quantity;
    }

    public int getIdClan() {
        return idClan;
    }

    public void setIdClan(int idClan) {
        this.idClan = idClan;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
