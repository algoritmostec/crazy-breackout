package game.model.main;


import java.io.Serializable;
import java.util.ArrayList;

import bohr.engine.GameEngine;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;

/**
 *Clase de cliente del juego encargada de los recursos y los clanes inicia el juego
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class ClientGame extends GameEngine implements Serializable {
    private static final long serialVersionUID = 1L;
    private Clan clan;
    private ArrayList<GameResource> resources;
    public ClientGame() {
        super();
        clan = new Clan();
        resources = new ArrayList<>();
    }

    public Clan getClan() {
        return clan;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ArrayList<GameResource> getResources() {
        return resources;
    }

    public void setResources(ArrayList<GameResource> resources) {
        this.resources = resources;
    }
    /*
    public void event(Object sender, String event, JSONObject obj) {
        switch (event) {
            case ServerProtocol.MESSAGES_COMMAND:
                notifyObservers(event, obj.toString());
                break;
            case ServerProtocol.MESSAGE_COMMAND:
                notifyObservers(event, obj.toString());
                break;
            default:
                break;
        }
    }*/

}
