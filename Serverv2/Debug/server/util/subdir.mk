################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../server/util/Observable.cpp \
../server/util/Observer.cpp \
../server/util/Util.cpp 

OBJS += \
./server/util/Observable.o \
./server/util/Observer.o \
./server/util/Util.o 

CPP_DEPS += \
./server/util/Observable.d \
./server/util/Observer.d \
./server/util/Util.d 


# Each subdirectory must supply rules for building sources it contributes
server/util/%.o: ../server/util/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


