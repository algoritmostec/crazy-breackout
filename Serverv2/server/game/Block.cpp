/*
 * Block.cpp
 *
 *  Created on: Mar 1, 2016
 *      Author: schrodingerscat
 */

#include "Block.h"

namespace std {

Block::Block(int id, int type) {
	this->id = id;
	this->active = true;
	this->content = type;
	this->posX = 0;
	this->posY = 0;
}

Block::Block() {
	id = -1;
	active = false;
	content = -1;
	this->posX = 0;
	this->posY = 0;
}

bool Block::isActive() const {
	return active;
}

void Block::setActive(bool active) {
	this->active = active;
}

int Block::getId() const {
	return id;
}

void Block::setId(int id) {
	this->id = id;
}

int Block::getType() const {
	return content;
}

void Block::setType(int type) {
	this->content = type;
}
Json::Value Block::toJson() {
	Json::Value block;
	block["id"] = id;
	block["active"] = active;
	block["content"] = content;
	block["posX"] = posX;
	block["posY"] = posY;
	return block;
}
Block::~Block() {
	// TODO Auto-generated destructor stub
}

} /* namespace std */

int std::Block::getPosX() const {
	return posX;
}

void std::Block::setPosX(int posX) {
	this->posX = posX;
}

int std::Block::getPosY() const {
	return posY;
}

void std::Block::setPosY(int posY) {
	this->posY = posY;
}
