/*
 * Block.h
 *
 *  Created on: Mar 1, 2016
 *      Author: schrodingerscat
 */

#ifndef BLOCK_H_
#define BLOCK_H_
#include <iostream>
#include "jsoncpp/json.h"
#include "jsoncpp/value.h"
namespace std {

class Block {
	int id;
	int content;
	bool active;
	int posX;
	int posY;
public:
	Block(int, int);
	Block();
	virtual ~Block();
	bool isActive() const;
	void setActive(bool active);
	int getId() const;
	void setId(int id);
	int getType() const;
	void setType(int type);
	Json::Value toJson();
	int getPosX() const;
	void setPosX(int posX);
	int getPosY() const;
	void setPosY(int posY);
};

} /* namespace std */
#endif /* BLOCK_H_ */
