/*
 * World.cpp
 *
 *  Created on: Mar 1, 2016
 *      Author: schrodingerscat
 */

#include "World.h"

World::World(int size) {
	this->size = size;
	blocks = new Block[size];
	this->initialize();
}
void World::initialize() {
	int id = 3;
	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 4; y++) {
			blocks[id - 3] = Block(id, id);
			blocks[id - 3].setPosX(x);
			blocks[id - 3].setPosY(y);
			id++;
		}
	}
}

int World::getSize() const {
	return size;
}

void World::setSize(int size) {
	this->size = size;
}

Json::Value World::toJson(int j) {
	cout << j << " - " << j + 10 << endl;
	Json::Value blocksList;
	for (int i = j; i < j + 10; i++) {
		if (blocks[i].isActive())
			blocksList.append(blocks[i].toJson());
	}
	if (blocksList.isNull())
		blocksList.append((new Block(-1, 0))->toJson());
	Json::Value world;
	world["size"] = size;
	world["blocks"] = blocksList;
	return world;
}

bool World::removeBlock(int id) {
	bool removed = false;
	for (int i = 0; i < size; i++) {
		if (blocks[i].getId() == id && blocks[i].isActive()) {
			blocks[i].setActive(false);
			removed = true;
		}
	}
	return removed;
}

World::~World() {
	/*for (int i = 0; i < sizeY; i++) {
	 free(blocks[i]);
	 }*/
	free(blocks);
}
