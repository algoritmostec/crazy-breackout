/*
 * World.h
 *
 *  Created on: Mar 1, 2016
 *      Author: schrodingerscat
 */

#ifndef WORLD_H_
#define WORLD_H_
#include <vector>
#include "Block.h"
#include <stdlib.h>
#include <iostream>
#include "jsoncpp/json.h"
#include "jsoncpp/value.h"
using namespace std;

class World {
private:
	Block* blocks;
	int size;
	void initialize();
public:
	World(int size);
	virtual ~World();
	const vector<class Block>& getBlocks() const;
	void setBlocks(const vector<class Block>& blocks);
	Json::Value toJson(int);
	int getSize() const;
	void setSize(int size);
	bool removeBlock(int);
};
#endif /* WORLD_H_ */
