/*
 * Socket.h
 *
 *  Created on: Feb 21, 2016
 *      Author: schrodingerscat
 */

#ifndef SOCKET_H_
#define SOCKET_H_
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include "poll.h"
using namespace std;

class Socket {
public:
	Socket(int, char*);
	Socket(int, sockaddr_in);
	virtual ~Socket();
	char* getAddress() const;
	void setAddress(char* address);
	int getFileDescriptor() const;
	void setFileDescriptor(int fileDescriptor);
	int getPort() const;
	void setPort(int port);
	const struct sockaddr_in& getAddressStruct() const;
	void setAddressStruct(const struct sockaddr_in& addressStruct);
	void readString(char* buffer[255]);
	void writeString(string);
	void streamString(char*);
	void close();
	bool isClose();

	bool charCompare(char* buffer, char* msg) {
		if (sizeof(buffer) == sizeof(msg)) {
			for (int i = 0; i < sizeof(buffer); i++) {
				if (buffer[i] != msg[i])
					return false;
			}
			return true;
		} else {
			return false;
		}
	}
protected:
	int fileDescriptor;
	char* address;
	struct sockaddr_in address_struct;
};

#endif /* SOCKET_H_ */
