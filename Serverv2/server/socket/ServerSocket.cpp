/*
 * ServerSocket.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: schrodingerscat
 */

#include "ServerSocket.h"

ServerSocket::~ServerSocket() {
	// TODO Auto-generated destructor stub
}

ServerSocket::ServerSocket(int port, char* ip) :
		Socket(port, ip) {
	waitingListSize = 5;
	util = new Util();
}

void ServerSocket::startListenning() {
	//Crea un socket y devuelve un identificador del mismo,
	// que lo representa dentro del proceso interno
	fileDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (fileDescriptor < 0)
		perror("ERROR opening socket");
	cout << "Server socket created" << endl;

	//************************
	bzero((char *) &address, sizeof(address));
	if (bind(fileDescriptor, (struct sockaddr *) &address_struct,
			sizeof(address_struct)) < 0)
		perror("ERROR on binding");
	cout << ("Address binded to socket") << endl;
	/*
	 inet_aton("63.161.169.137", &myaddr.sin_addr.s_addr);

	 s = socket(PF_INET, SOCK_STREAM, 0);
	 bind(s, (struct sockaddr*)myaddr, sizeof(myaddr));
	 */
	//************************
	listen(fileDescriptor, waitingListSize);
	cout << ("Server socket listening") << endl;
}

Socket* ServerSocket::acceptClient() {
	struct sockaddr_in client_socket_settings;
	unsigned int clLen = sizeof(client_socket_settings);
	int clFileDesc = accept(fileDescriptor,
			(struct sockaddr*) &client_socket_settings, &clLen);
	if (clFileDesc < 0)
		perror("ERROR on accept");
	cout << ("Client accepted") << endl;
	return new Socket(clFileDesc, client_socket_settings);

}

