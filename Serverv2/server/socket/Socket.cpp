/*
 .25* * Socket.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: schrodingerscat
 */

#include "Socket.h"

Socket::Socket(int port, char* address) {
	this->address = address;
	this->fileDescriptor = -1;
	this->address_struct.sin_family = AF_INET;
	this->address_struct.sin_addr.s_addr = htonl(INADDR_ANY);
	this->address_struct.sin_port = htons(port);
}
Socket::Socket(int fileDescriptor, sockaddr_in socket) {
	this->address = NULL;
	this->fileDescriptor = fileDescriptor;
	this->address_struct = socket;
}

Socket::~Socket() {
}

char* Socket::getAddress() const {
	return address;
}

void Socket::setAddress(char* address) {
	this->address = address;
}

int Socket::getFileDescriptor() const {
	return fileDescriptor;
}

void Socket::setFileDescriptor(int fileDescriptor) {
	this->fileDescriptor = fileDescriptor;
}

int Socket::getPort() const {
	return this->address_struct.sin_port;
}

void Socket::setPort(int port) {
	this->address_struct.sin_port = htons(port);
}

const struct sockaddr_in& Socket::getAddressStruct() const {
	return address_struct;
}

void Socket::setAddressStruct(const struct sockaddr_in& addressStruct) {
	this->address_struct = addressStruct;
}

void Socket::readString(char* buffer[255]) {
	bzero(buffer, 256);
	int statusRead = read(fileDescriptor, buffer, 255);
	if (statusRead < 0)
		cout << "ERROR reading from socket" << endl;
	string bufferStr((char*) buffer);
	cout << "Request from client (" << this->address_struct.sin_addr.s_addr
			<< ") :(" << sizeof(buffer) << "bytes) -> " << bufferStr << endl;
}

void Socket::writeString(string blocksStr) {
	int statusWrite = 0;
	try {
		statusWrite = write(fileDescriptor, blocksStr.c_str(),
				blocksStr.size());
	} catch (exception e) {
		//cout << e.what() << endl;
	}
	/*if (statusWrite < 0)
		perror("ERROR writing to socket");*/
	cout << "Response to client (" << this->address_struct.sin_addr.s_addr
			<< ") : (" << blocksStr.size() << "bytes) -> " << blocksStr << endl;
}
void Socket::streamString(char* buffer) {
	int statusWrite = write(fileDescriptor, buffer, sizeof(buffer));
	/*if (statusWrite < 0)
		perror("ERROR writing to socket");*/
	cout << "Response to client (" << this->address_struct.sin_addr.s_addr
			<< ") :" << buffer << " -> " << sizeof(buffer) << endl;
}

void Socket::close() {
	shutdown(fileDescriptor, 2);
}
bool Socket::isClose() {
	pollfd pfd;
	pfd.fd = fileDescriptor;
	pfd.events = POLLIN | POLLHUP | POLLRDNORM;
	pfd.revents = 0;
	while (pfd.revents == 0) {
		// call poll with a timeout of 100 ms
		if (poll(&pfd, 1, 100) > 0) {
			// if result > 0, this means that there is either data available on the
			// socket, or the socket has been closed
			char buffer[32];
			if (recv(fileDescriptor, buffer, sizeof(buffer),
			MSG_PEEK | MSG_DONTWAIT) == 0) {
				// if recv returns zero, that means the connection has been closed:
				return true;
			}
		}
	}
	return false;
}
