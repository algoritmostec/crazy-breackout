/*
 * ServerSocket.h
 *
 *  Created on: Feb 21, 2016
 *      Author: schrodingerscat
 */

#ifndef SERVERSOCKET_H_
#define SERVERSOCKET_H_
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include "Socket.h"
#include "../util/Util.h"

class ServerSocket: private Socket {
public:
	ServerSocket(int, char*);
	virtual ~ServerSocket();
	void startListenning();
	int responseSocket(int, char*);
	Socket* acceptClient();
private:
	int waitingListSize;
	Util* util;
};

#endif /* SERVERSOCKET_H_ */
