/*

 * Server.h
 *
 *  Created on: Feb 21, 2016
 *      Author: schrodingerscat
 */

#ifndef SERVER_H_
#define SERVER_H_
#include "socket/ServerSocket.h"
#include "util/Observer.h"
#include "util/Observable.h"
#include <unistd.h>

class Server: public Observable {
public:
	Server();
	virtual ~Server();
	void run(void *threadid);
	void start();
private:
	bool active;
	ServerSocket *serverSocket;
};

#endif /* SERVER_H_ */
