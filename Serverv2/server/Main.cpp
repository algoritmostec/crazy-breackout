//============================================================================
// Name        : Server.cpp
// Author      : Randall Alfaro
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <list>
#include <string>
#include "Server.h"
#include "util/Observer.h"
#include "socket/Socket.h"
#include "game/World.h"
#include "jsoncpp/json.h"
#include "jsoncpp/value.h"
#include "jsoncpp/reader.h"
#include "jsoncpp/writer.h"
#include <jsoncpp/json/value.h>
#include "util/events/Event.h"
using namespace std;
//************************************************************************
struct clientInfo {
	Socket* socket;
	int type;
	int id;
	//ThreadedResponse* threadedResponse;
};
struct responseInfo {
	World* world;
	pthread_mutex_t mutex_block;
	pthread_mutex_t mutex_id;
	int* currentId;
	list<clientInfo*> clients;
	pthread_mutex_t mutex_notification;
};
struct responseParam {
	responseInfo* data;
	Socket* socket;
};
//************************************************************************

class ThreadedResponse {
private:
	pthread_t thread;
public:
	ThreadedResponse(responseInfo* data, Socket* socket) {
		responseParam* param = new responseParam();
		param->data = data;
		param->socket = socket;
		pthread_create(&thread, NULL, &ThreadedResponse::readRequests, param);
		pthread_detach(thread);
	}

	static void *readRequests(void* params) {
		responseParam* data = (responseParam*) params;
		try {
			Util util = Util();
			clientInfo *clientData = new clientInfo();
			clientData->socket = (Socket*) data->socket;
			cout << "<readRequests>" << endl;
			char* buffer[255] = { };
			Json::Value root;
			Json::Reader reader;
			string bufferStr((char*) buffer);
			bool parsedSuccess = reader.parse(bufferStr, root, false);
			while (strcmp((char*) buffer, "exit") != 0
					&& !data->socket->isClose()) {
				data->socket->readString(buffer);
				string bufferStr((char*) buffer);
				Json::Value root;
				Json::Reader reader;
				parsedSuccess = reader.parse(bufferStr, root, false);
				cout << "--------------------" << parsedSuccess << endl;
				if (parsedSuccess == true && root.type() != 0
						&& root["event"].isNull() != true) {
					Event event = Event();
					if (root["userId"].isNull() != true
							&& strcmp(root["event"].asCString(), "join session")
									== 0) {
						clientData->type = 0;
						event.setEvent("join session");
						pthread_mutex_lock(&data->data->mutex_id);
						*(data->data->currentId) = *(data->data->currentId) + 1;
						event.setUserId(*(data->data->currentId));
						clientData->id = *(data->data->currentId);
						data->data->clients.push_back(clientData);
						pthread_mutex_unlock(&data->data->mutex_id);
						data->socket->writeString(
								event.toJson().toStyledString());
						notify(event.toJson().toStyledString(), data,
								root["userId"].asInt());
					} else if (strcmp(root["event"].asCString(), "notification")
							== 0) {
						clientData->type = 1;
						data->data->clients.push_back(clientData);
					} else if (strcmp(root["event"].asCString(), "get clients")
							== 0) {
						Json::Value clientsJson;
						std::list<clientInfo*>::iterator iterator;
						list<clientInfo*> clients = data->data->clients;
						for (iterator = clients.begin();
								iterator != clients.end(); ++iterator) {
							if ((*iterator)->type == 0)
								clientsJson.append((*iterator)->id);
						}
						clientsJson.append(-1);
						data->socket->writeString(clientsJson.toStyledString());

					} else if (root["type"].isNull() != true
							&& strcmp(root["event"].asCString(), "get world")
									== 0) {
						if (strcmp(root["type"].asCString(), "1") == 0) {
							data->socket->writeString(
									data->data->world->toJson(0).toStyledString());
						} else if (strcmp(root["type"].asCString(), "2") == 0) {
							data->socket->writeString(
									data->data->world->toJson(10).toStyledString());
						} else if (strcmp(root["type"].asCString(), "3") == 0) {
							data->socket->writeString(
									data->data->world->toJson(20).toStyledString());
						} else if (strcmp(root["type"].asCString(), "4") == 0) {
							data->socket->writeString(
									data->data->world->toJson(30).toStyledString());
						}
					} else if (root["blockId"].isNull() != true
							&& root["userId"].isNull() != true
							&& strcmp(root["event"].asCString(), "hit block")
									== 0) {
						pthread_mutex_lock(&data->data->mutex_block);
						if (data->data->world->removeBlock(
								root["blockId"].asInt())) {
							event.setEvent("ok");
							data->socket->writeString(
									event.toJson().toStyledString());
							notify(root.toStyledString(), data,
									root["userId"].asInt());
						} else {
							event.setEvent("error");
							data->socket->writeString(
									event.toJson().toStyledString());
						}
						pthread_mutex_unlock(&data->data->mutex_block);
					} else if (root["userId"].isNull() != true
							&& strcmp(root["event"].asCString(), "ball hit")
									== 0
							|| strcmp(root["event"].asCString(), "bar move")
									== 0
							|| strcmp(root["event"].asCString(), "game over")
									== 0) {
						event.setEvent("ok");
						data->socket->writeString(
								event.toJson().toStyledString());
						notify(root.toStyledString(), data,
								root["userId"].asInt());
					} else {
						event.setEvent("error");
						data->socket->writeString(
								event.toJson().toStyledString());
					}
				}
			}
		} catch (exception ex) {

		}
		data->socket->close();
		cout << "socket closed" << endl;
		return 0;
	}

	static void notify(string notification, responseParam* param, int id) {
		std::list<clientInfo*>::iterator iterator;
		list<clientInfo*> clients = param->data->clients;
		pthread_mutex_lock(&(param->data->mutex_notification));
		for (iterator = clients.begin(); iterator != clients.end();
				++iterator) {
			if ((*iterator)->type == 1 && (*iterator)->id != id) {
				//cout << "NOTIFICATION: " << notification << endl;
				(*iterator)->socket->writeString(notification);
			}
		}
		pthread_mutex_unlock(&(param->data->mutex_notification));
	}

	pthread_t getThread() const {
		return thread;
	}
	//borrar bloque mutex

};

//***************************************************************************
class Route: public Observer {
private:
	responseInfo* data;
	int id;
	void update(void* param) {
//clean();
		/*clientData->threadedResponse = */
		new ThreadedResponse(data, (Socket*) param);
	}
	void clean() {
		std::list<clientInfo*>::iterator iterator;
		for (iterator = data->clients.begin(); iterator != data->clients.end();
				++iterator) {
			if ((*iterator)->socket->isClose()) {
				//std::terminate((*iterator)->threadedResponse->getThread());
				data->clients.erase(iterator);
			}
		}
	}
public:
	Route() {
		//data->clients = list<clientInfo*>();
		data = new responseInfo();
		id = 0;
		data->currentId = &id;
		data->world = new World(40);

	}
};

//***************************************************************************
int main() {
	Server *server = new Server();
	server->attach(new Route());
	cout << "MAIN" << endl;
	server->start();
	return 0;
}
