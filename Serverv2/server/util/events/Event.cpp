/*
 * Event.cpp
 *
 *  Created on: Mar 8, 2016
 *      Author: schrodingerscat
 */

#include "Event.h"

Event::Event() {
	// TODO Auto-generated constructor stub

}

const string& Event::getEvent() const {
	return event;
}

void Event::setEvent(const string& event) {
	this->event = event;
}

void Event::setType(const string& type) {
	this->type = type;
}

const string& Event::getType() const {
	return type;
}

void Event::setUserId(int userId) {
	this->userId = userId;
}

Event::~Event() {
	// TODO Auto-generated destructor stub
}

Json::Value Event::toJson() {
    Json::Value root;
    root["event"] = event;
    root["type"] = type;
    root["userId"] = userId;
    return root;
}
