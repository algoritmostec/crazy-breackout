/*
 * HitEvent.h
 *
 *  Created on: Mar 8, 2016
 *      Author: schrodingerscat
 */

#ifndef EVENTS_HITEVENT_H_
#define EVENTS_HITEVENT_H_
#include "Event.h"
using namespace std;

class HitEvent: public Event {

public:
	HitEvent();
	virtual ~HitEvent();
};

#endif /* EVENTS_HITEVENT_H_ */
