/*
 * Event.h
 *
 *  Created on: Mar 8, 2016
 *      Author: schrodingerscat
 */

#ifndef EVENTS_EVENT_H_
#define EVENTS_EVENT_H_
#include <string>
#include "jsoncpp/value.h"
#include "jsoncpp/json.h"
using namespace std;

class Event {
private:
	string event;
	string type;
	int userId;
public:
	Event();
	virtual ~Event();
	const string& getEvent() const;
	void setEvent(const string& event);
	int getUserId() const;
	void setUserId(int userId);
	Json::Value toJson() ;
	const string& getType() const;
	void setType(const string& type);
};

#endif /* EVENTS_EVENT_H_ */
