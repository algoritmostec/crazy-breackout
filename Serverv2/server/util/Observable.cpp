/*
 * Observable.cpp
 *
 *  Created on: Feb 29, 2016
 *      Author: schrodingerscat
 */

#include "Observable.h"

Observable::Observable() {
	views = {};
}

Observable::~Observable() {
}

void Observable::notify(void* param) {
	for (int i = 0; i < views.size(); i++)
		views[i]->update(param);
}
void Observable::attach(Observer *obs) {
	views.push_back(obs);
}

