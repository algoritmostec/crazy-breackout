/*
 * Observable.h
 *
 *  Created on: Feb 29, 2016
 *      Author: schrodingerscat
 */

#ifndef OBSERVABLE_H_
#define OBSERVABLE_H_
#include <iostream>
#include <vector>
#include "Observer.h"

using namespace std;
class Observable {
	vector<class Observer*> views; // 3. Coupled only to "interface"
	int value;
public:
	void notify(void * param);
	void attach(Observer *obs);
	void setVal(int val);
	int getVal();
	Observable();
	virtual ~Observable();
};

#endif
/* OBSERVABLE_H_ */
