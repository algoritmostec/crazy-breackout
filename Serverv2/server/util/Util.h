/*
 * Util.h
 *
 *  Created on: Mar 1, 2016
 *      Author: schrodingerscat
 */

#ifndef UTIL_H_
#define UTIL_H_
#include <string>
#include <cstring>
#include <iostream>
namespace std {

class Util {
public:
	Util();
	virtual ~Util();
	bool charCompare(char** buffer, char** msg) {
		if (sizeof(buffer) == sizeof(msg)) {
			for (int i = 0; i < sizeof(buffer); i++) {
				if (buffer[i] != msg[i])
					return false;
			}
			return true;
		}
		return false;
	}
	bool charCompare(char* buffer, char* msg) {
		if (sizeof(buffer) == sizeof(msg)) {
			for (int i = 0; i < sizeof(buffer); i++) {
				if (buffer[i] != msg[i])
					return false;
			}
			return true;
		} else {
			return false;
		}
	}
	bool charStringCompare(char* buffer, string* msg) {
		char charConvert[1024];
		strcpy(charConvert, msg->c_str());
		return charCompare(buffer, charConvert);
	}
};

} /* namespace std */
#endif /* UTIL_H_ */
