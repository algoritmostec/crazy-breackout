/*
 * Observer.h
 *
 *  Created on: Mar 1, 2016
 *      Author: schrodingerscat
 */

#ifndef OBSERVER_H_
#define OBSERVER_H_

namespace std {

class Observer {
public:
	Observer();
	virtual ~Observer();
	virtual void update(void* param)=0;
};

} /* namespace std */
#endif /* OBSERVER_H_ */
