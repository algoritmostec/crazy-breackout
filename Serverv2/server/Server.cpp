/*
 * Server.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: schrodingerscat
 */

#include "Server.h"

Server::Server() {
	active = true;
	serverSocket = new ServerSocket(8080, "127.0.0.1");
}

Server::~Server() {
}
/*
 static void* Server::run(void *threadid) {
 //serverSocket->startListenning();
 //file
 while(active){
 //ClientSocket clientSocket = serv
 int request = clientSocket.readInt();
 clientSocket.send(1);
 clientSocket.close();
 }
 }*/
void Server::start() {
	/*pthread_t thread ;
	 int thread_id = 1;
	 pthread_create(&thread, NULL, &Server::run, NULL);*/
	serverSocket->startListenning();
	while (active) {
		cout << endl << "-------------------------------------------" << endl;
		Socket* client = serverSocket->acceptClient();
		notify(client);
		usleep(20);
	}
}
